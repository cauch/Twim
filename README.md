This
World
Is
Magic

A small game in python/pygame

# screenshots:

![Screenshot1](/doc/screenshot1.png)

![Screenshot2](/doc/screenshot2.png)

# dependancies:

pygame,

twisted

# how to run:

cd game;

python main.py
