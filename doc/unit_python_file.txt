The classical python file is the following.
It is composed of 1 class, called Unit
This class has several variables:

NAME:
unique name of the unit

IMAGES:
list of images, located in the same directory, that will be loaded
for a NAME X and an image named Y, the image will be refered to u_X_Y

RARETY:
how rare is the unit, the higher, the rarer
the formula to keep this unit as possible in the option is: randint(0,10+rarety)<10
it means 0 -> 100%, 1 -> 99%, 2 -> 83%, 3 -> 76%, 4 -> 71%, 5 -> 66%, 10 -> 50%, 20 -> 33%, 30 -> 25%, 40 -> 20%, 100 -> 10%... 

TYPE:
list of type it can be
0: just an object, persistent, created with the room (max 1 per room)
1: another object, but can be destroyed and can be created
2: creature that attack and defend, but don't travel
3: creature that attack and defend, and travel
4: can be played by the player

The __init__ contain the parent
If it is an unit, it's the world class in twim_world
It it is a Decor, it's the object class in twim_object

The Load is where the graphics are created
The argument are the position (left, center or right) and the orientation (0
to the left, 1 to the right)
The graphics are based on what done in twim_object.
More info on that on twim_object.txt 
The object NEEDS to be named starting with "u0_" or "u1_" or "u2_", the number
being the position.

The Description is what is returned when the description is asked
It can use self.parent to access to the world
It can use info to get the characteristic of the object
info is a dictionnary containing the important properties of the objects
The syntax of the text is using the interface syntax, cf text_interface.txt

The Fill is what is called to initialize the unit
It usually uses predefined function from twim_world, but allows for more
customization of an unit


import random
from math import *
import time

class Unit:

    NAME="chest"
    RARETY=0
    TYPE=[0]
    IMAGES=["chest0.png"]

    def __init__(self,parent):
        self.parent = parent
        #parent can be twim_object or twim_world
        #depending on the function called

    def Load(self,i,o):
        #i: 0 = left, 1 = center, 2 = right
        #   i is also used in pos3[i] and unit_obj[i]
        #o: 0 is to the left, 1 is to the right
        parent = self.parent
        pos = parent.pos3[i]
        mod = {"S":1.1,"A":0,"I":0,"T":0}
        parent.Set_object("u"+repr(i)+"_chest"+repr(o),"u_chest_chest0",[pos[0],pos[1]],[28,42],mod)

    def Description(self,info):
        text = "this is a chest$n"
        text += "$n"
        sp = info.get("spells",[])
        if sp:
            text += "it contains the word$n"
            text += "$b%s$e"%sp[0]
        else:
            text += "it is empty"
        return text

    def Fill(self, unit, room):
        w = room.world
        sp = w.Find_spell([3,4,5],[0,1,2,3],room.style[0])
        if sp:
            unit.spells.append(sp.name)
