#    This file is part of TWIM.
#
#    TWIM is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    TWIM is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with TWIM.  If not, see <http://www.gnu.org/licenses/>.

"""
Used for twim_display

In the display, the environment is drawn.
Different drawing are available, called 'decor'
In the display, the different units are drawn/
Those units are called 'unit'

This class will load the decors and units from
the 'decors' and 'units' directories
And provides basic function that those objects
need
"""

"""
Modification syntax:
normalname,Sx,Ax,Ix,Tx

S: scale (2 digits)
A: deciangle (int 0->1440 (%360 does the trick) )
I: inverted (0 or 1)
T: transparency (percent, 2 digits)
"""

import pygame
from pygame.locals import *
import random
from math import *
import time
import os

class Motion:
    def __init__(self,ot,actions):
        """
        You can define a motion, that will be applied to a list of object names (self.list_object)
        Each object can have parameters for this motion (self.param_object)
        A motion is a list of action to be done (self.actions)
        The motion are stored in objecttool and are named
        """
        self.ot = ot
        self.list_object = []
        self.param_object = {}
        self.param_global = {}
        self.actions = actions
        self.time_to_action = []
        self.time_shift = -1*ot.motion_timer
        self.dont_kill = 0#if =1, the motion will not be deleted when the decor is changed
        self.Set_time_to_action()
        #actions:
        #"loop": reset the time to 0
        #"wait": wait the time in "time"
        #"mod": modify the current image with the modificator in "mod"
        #"random_wait": wait the time: "time1" + randint(0,"time2")
        #"fct": execute the function in "fct"
        #"empty": nothing (wait for time 1)
        #"move": move by a step of "x,y"
        #"anim": specific animation of name "anim", of step "step" (the current position is given by "i" in param_object)

    def Set_time_to_action(self):
        if self.time_to_action != []:
            return
        for ia in range(len(self.actions)):
            a = self.actions[ia]
            t = a.get("type","")
            if t in ["mod","loop","fct","empty","move","erase","amove"]:
                self.time_to_action.append(ia)
                if a.get("repeat"):
                    for i in range(int(a.get("repeat"))-1):
                        self.time_to_action.append(ia)
            if t == "wait":
                for i in range(a.get("time",1)+1):
                    self.time_to_action.append(ia)
            if t == "anim":
                for i in range(a.get("step",1)):
                    self.time_to_action.append(ia)
            if t == "random_wait":
                time = 1+a.get("time1",1)+random.randint(1,a.get("time2",1))
                for i in range(time):
                    self.time_to_action.append(ia)

    def Run(self):
        r = 0
        for obn in self.list_object:
            param = self.param_global.copy()
            #if there is additional param specific to the object, add them
            #if the param exists already, then add the value
            po = self.param_object.get(obn,{})
            for k in po:
                if k in param:
                    param[k] += po[k]
                else:
                    param[k] = po[k]
            r += self.Run_object(obn,param)
        if not self.list_object:
            param = self.param_global.copy()
            r += self.Run_object("",param)
        return r

    def Run_object(self,obn,param):
        mtime = self.ot.motion_timer-1
        delay = param.get("delay",0)
        mtime += delay
        mtime += self.time_shift
        obj = None
        if obn:
            obj = self.ot.Get_obj_objfromname(obn)
        a = None
        if mtime >= 0 and mtime < len(self.time_to_action):
            a = self.actions[self.time_to_action[mtime]]
        elif mtime < 0:
            return 1
        if a:
            t = a.get("type","")
            if t == "loop":
                self.time_shift = -1*self.ot.motion_timer
                self.time_to_action = []
                self.Set_time_to_action()
            elif t == "mod":
                if obj:
                    mod = a.get("mod",{})
                    #print "mod",mod,obj.image_name
                    obj.Apply_motion_relativemod(mod,param)
            elif t == "move":
                if obj:
                    xy1 = a.get("x,y",[0,0])
                    xy2 = param.get("x,y",[0,0])
                    obj.Apply_motion_relativemove([xy1[0]+xy2[0],xy1[1]+xy2[1]],param)
            elif t == "amove":
                if obj:
                    xy = a.get("x,y",[0,0])
                    obj.Apply_motion_absolutemove(xy,param)
            elif t == "fct":
                ff = a.get("fct",None)
                if ff: ff(obn)
            elif t == "anim":
                if obj:
                    self.Do_anim(a,obj,param)
            elif t == "erase":
                if obj:
                    do = obj.object_name
                    self.ot.decor_obj[do].kill()
                    del self.ot.decor_obj[do]
            return 1
        else:
            return 0

    def Do_anim(self,a,obj,param):
        anim = a.get("anim","")
        step = a.get("step",1)
        if obj.object_name not in self.param_object:
            self.param_object[obj.object_name] = {}
        op = self.param_object[obj.object_name]
        i = op.get("i",0)
        if i >= step:
            i = 0
        op["i"] = i+1
        if anim in ["appear","disappear"]:
            #motion from 0 to 300
            step_x = []
            for ii in range(step):
                step_x.append(0)
            dx = -1
            if anim == "disappear": dx = 1
            for ii in range(300):
                step_x[ii%step] += dx
            if len(step_x) >= 4:
                dx = (step_x[0]*3)/4
                step_x[0] = step_x[0]-dx
                step_x[-1] = step_x[-1]+dx
                dx = (step_x[1]*1)/2
                step_x[1] = step_x[1]-dx
                step_x[-2] = step_x[-2]+dx
            if anim == "disappear": 
                step_x.reverse()
            xy = [0,step_x[i]]
            #print "anim",anim,i,xy
            obj.Apply_motion_relativemove(xy,param)
        else:
            print "Warning, motion anim of type %s is not implemented"%anim


class Object(pygame.sprite.Sprite):
    def __init__(self,ot,object_name,image_name,image_anchor,position):
        """
        Second layer

        arguments:
           ot: object tool
           object_name: name of the object
           image_name: key for the display.images[key] (if it does not exist, it is create via ot.Get_image)
           image_anchor: position on the anchor on the image_name
           position: position of the object in the screen
        """
        self.ot = ot
        self.object_name = object_name
        self.image_name = image_name
        self.image_name_or = image_name
        self.image_anchor_or = image_anchor[:]
        self.position = position

        pygame.sprite.Sprite.__init__(self, self.containers)
        self.rect = pygame.Rect(0,0,0,0)
        
        self.motion_param = {}

        #instead of recomputing those value several times
        #I should check if it's better to spend memory or cpu power
        self.memory_enable = 1
        self.memory_anchor = {}
        self.memory_mod = {}

        self.to_refresh = 1
        self.first_draw = 0
        
        #object can be composed
        #in case of composed object, children contains the list of Object name link + their position
        #example: [["subobject1",0,0],["subobject2",10,10]]
        #when the object is modified, the modification is propagated to the child
        #the initial object name is put in root
        #please use: Add_child(object,pos)
        self.root = ""
        self.children = []

        self.Redraw()

    def Redraw(self):
        self.image = self.Get_image(self.image_name)
        self.image_anchor = self.Modify_anchor(self.image_anchor_or,self.image_name)        
        self.rect = self.image.get_rect()
        pos = self.Get_position(self.position)
        self.rect.left = int(round(pos[0]))
        self.rect.top = int(round(pos[1]))


    def Get_position(self,pos):
        #adjust the top-left position so
        #that the anchor is in position pos
        x = pos[0]-self.image_anchor[0]
        y = pos[1]-self.image_anchor[1]
        return [x,y]

    def Get_image(self,name):
        if name in self.ot.display.images.keys():
            return self.ot.display.images[name]
        else:
            sp = name.split(",")
            if len(sp) == 5:
                S,A,I,T = float(sp[1][1:]),int(sp[2][1:]),int(sp[3][1]),int(sp[4][1:])
                mod = {"S":S,"A":A,"I":I,"T":T}
                or_image = self.ot.display.images[sp[0]]
                im = self.Modify_image(or_image,mod)
                self.ot.display.images[name] = im
                #print "create a new image",name,"total",len(self.ot.display.images)
                return im
            else:
                print "Image not found",name
        
    def Modify_image(self,or_image,mod):
        S,A,I,T = self.ot.Get_obj_varfrommod(mod)
        self.old_rect = or_image.get_rect()
        i1 = or_image.copy()
        if I==1:
            i1 = pygame.transform.flip(or_image,True,False)
        if A!=0:
            i1 = pygame.transform.rotate(i1,A/4.)
        if S!=1:
            w = int(i1.get_rect().width*S)
            h = int(i1.get_rect().height*S)
            i1 = pygame.transform.smoothscale(i1,(w,h))
        if T!=0:
            alpha = int(2.55*T)
            if T > 100:
                alpha = 255
            alpha = 255-alpha
            i1.fill((255, 255, 255, alpha), None, pygame.BLEND_RGBA_MULT)
        self.new_rect = i1.get_rect()
        return i1

    def Get_varfromname(self,name):
        sp = name.split(",")
        if len(sp) == 5:
            na,oS,oA,oI,oT = sp[0],float(sp[1][1:]),int(sp[2][1:]),int(sp[3][1]),int(sp[4][1:])
        else:
            na = sp[0]
            oS,oA,oI,oT = self.ot.Get_obj_varfrommod({})
        return na,oS,oA,oI,oT

    def Modify_name(self,or_name,mod,amp=1):
        tm = ""
        #if self.memory_enable:
        #    tm = repr(mod)+repr(amp)
        #    if tm in self.memory_mod:
        #        return self.memory_mod[tm]
        na,oS,oA,oI,oT = self.Get_varfromname(or_name)
        S,A,I,T = self.ot.Get_obj_varfrommod(mod)
        if S != 1:
            S = S*amp
        rS,rA,rI,rT = round(oS*S,2),round(oA+(amp*A))%1440,(oI+I)%2,(oT+(T*amp))#/100
        if rT < 0: rT=0
        newname = self.ot.Get_obj_namefrommod(na,{"S":rS,"A":rA,"I":rI,"T":rT})
        #if self.memory_enable:
        #    self.memory_mod[tm] = newname
        return newname

    def Modify_anchor(self,or_anchor,name):

        mt = ""
        if self.memory_enable:
            mt = "%s_%i_%i"%(name,or_anchor[0],or_anchor[1])
            if mt in self.memory_anchor:
                return self.memory_anchor[mt]

        na,S,A,I,T = self.Get_varfromname(name)

        pos = or_anchor[:]
        ow = self.ot.display.images[na].get_rect().width
        oh = self.ot.display.images[na].get_rect().height
        if I==1:
            pos[0] = ow-pos[0]
        if A!=0:

            #dx,dy for recentering the image
            nw = self.ot.display.images[name].get_rect().width
            nh = self.ot.display.images[name].get_rect().height
            #dx = (ow/2.)-(nw/2.)
            #dy = (oh/2.)-(nh/2.)

            rx,ry = (ow/2.)-pos[0],(oh/2.)-pos[1]
            a = A*-3.1416/(4*180.)
            nx,ny = (cos(a)*rx)-(sin(a)*ry),(sin(a)*rx)+(cos(a)*ry)

            #ow1,oh1 = (cos(a)*ow)-(sin(a)*oh),(sin(a)*ow)+(cos(a)*oh)
            #ow2,oh2 = (cos(a)*ow)-(sin(a)*0),(sin(a)*ow)+(cos(a)*0)
            #ow3,oh3 = (cos(a)*0)-(sin(a)*oh),(sin(a)*0)+(cos(a)*oh)
            ow = nw/S#max(abs(ow1),abs(ow2),abs(ow3))
            oh = nh/S#max(abs(oh1),abs(oh2),abs(oh3))
            pos = [int(((ow/2.)-nx)),int(((oh/2.)-ny))]
        if S!=1:
            pos = [int(pos[0]*S),int(pos[1]*S)]

        if self.memory_enable:
            self.memory_anchor[mt] = pos[:]
        return pos[:]

    def Apply_motion_relativemod(self,mod,param):
        ampl = param.get("intensity",1)
        ampl *= param.get("ampl",1)
        #initial_name = self.image_name
        #na,S,A,I,T = self.Get_varfromname(initial_name)
        image_name = self.Modify_name(self.image_name,mod,ampl)
        self.image_name = image_name
        self.Redraw()
        for ch in self.children:
            obj = self.ot.Get_obj_objfromname(ch)
            obj.Apply_motion_relativemod(mod,param)

    def Apply_motion_relativemove(self,xy,param):
        ampl = param.get("intensity",1)
        ampl *= param.get("ampl",1)
        self.position[0] = self.position[0]+(round(ampl*xy[0]))
        self.position[1] = self.position[1]+(round(ampl*xy[1]))
        self.Redraw()
        for ch in self.children:
            obj = self.ot.Get_obj_objfromname(ch)
            obj.Apply_motion_relativemove(xy,param)

    def Apply_motion_absolutemove(self,xy,param):
        self.position[0] = xy[0]
        self.position[1] = xy[1]
        self.Redraw()
        for ch in self.children:
            obj = self.ot.Get_obj_objfromname(ch)
            obj.Apply_motion_absolutemove(xy,param)
            
    def Add_child(self,obj_name,pos):
        obj = self.ot.Get_obj_objfromname(obj_name)
        if obj:
            obj.Set_root(self.object_name)
            self.children.append(obj_name)
        else:
            print "You should create all the children before add them"
        if self.root == "":
            self.root = self.object_name

    def Set_root(self,root_name):
        for l in self.children:
            obj = self.ot.Get_obj_objfromname(l)
            if obj:
                obj.Set_root(root_name)
        self.root = root_name
            

    def Refresh(self):
        if self.to_refresh == 0:
            return

        self.Redraw()

class ObjectU(Object):
    pass
    #Object for unit.
    #This is needed because they are using a new sprite group

class Object_tool:
    def __init__(self,display):
        self.display = display
        self.decor_modules = {}
        self.unit_modules = {}
        self.images_to_load = {}
        self.Load_decor()
        self.Load_unit()
        self.decor_obj = {}
        self.unit_obj = [{},{},{}]
        self.pos3 = [[0,0],[0,0],[0,0]]
        self.motions = {}
        self.motion_timer = 0

    def Load_decor(self):
        """
        Called once by Object_tool.__init__
        Add the decor_modules
        Add the images to load for the decor
        """
        self.decor_modules = self.display.client.modules_decor
        for d in self.decor_modules:
            path = self.decor_modules[d].PATH
            images = self.decor_modules[d].IMAGES
            for i in images:
                ii = i.split("/")[-1].split(".")[0]
                ti = "d_%s_%s"%(d,ii)
                self.images_to_load[ti] = os.path.join(path,i)

    def Load_unit(self):
        """
        Called once by Object_tool.__init__
        Add the unit_modules
        Add the images to load for the unit
        """
        self.unit_modules = self.display.client.modules_unit
        for u in self.unit_modules:
            path = self.unit_modules[u].PATH
            images = self.unit_modules[u].IMAGES
            for i in images:
                ii = i.split("/")[-1].split(".")[0]
                ti = "u_%s_%s"%(u,ii)
                self.images_to_load[ti] = os.path.join(path,i)

    def Load_bkgd(self):
        """
        Called once by Object_tool.Load_images
        Will create 6 new images, with the colours of the display.cl
        """
        d = self.display
        for style_cl in range(6):
            b = pygame.Surface(self.display.screenrect.size)
            b = b.convert_alpha()
            cl = d.cl[style_cl]
            b.fill( (0,0,0) )
            r = pygame.Rect(10,10,d.screenrect.width-20,d.screenrect.height-20)
            pygame.draw.rect( b, ( (cl[0]+200)/2, (cl[1]+200)/2, (cl[2]+200)/2 ), r)
            d.images["bkgd_%i"%style_cl] = b


    def Load_images(self):
        """
        Called by twim_display during initialization,
        to load of the png
        """
        d = self.display
        for i in self.images_to_load:
            d.images[i] = d.Load_pixmap(self.images_to_load[i])
        self.Load_bkgd()

    def Refresh(self):
        """
        Refresh of the object for unit and decor
        """
        self.motion_timer += 1
        k = self.motions.keys()
        for motion in k:
            r = self.motions[motion].Run()
            if not r:
                del self.motions[motion]
        for ok in self.decor_obj:
            self.decor_obj[ok].Refresh()
        for lok in self.unit_obj:
            for ok in lok:
                lok[ok].Refresh()

    def Reset(self,toreset="a"):
        """
        Kill the object when something changes.
        a: all
        d: decor only
        u: unit only
        u0,u1,u2: unit0 or unit1 or unit2 only
        """
        if "*" in toreset or "a" in toreset:
            toreset = "bdu"

        if "d" in toreset:
            dok = self.decor_obj.keys()
            for do in dok:
                self.decor_obj[do].kill()
                del self.decor_obj[do]
            #clean images
            ik = self.display.images.keys()
            for iki in ik:
                if iki.startswith("d_") and iki.count(","):
                    del self.display.images[iki]
            #clean motion
            k = self.motions.keys()
            for kk in k:
                if self.motions[kk].dont_kill == 0:
                    del self.motions[kk]

        ui = []
        if "u0" in toreset:
            ui.append(0)
        if "u1" in toreset:
            ui.append(1)
        if "u2" in toreset:
            ui.append(2)
        if ui == [] and "u" in toreset:
            ui = [0,1,2]
        for i in ui:
            uok = self.unit_obj[i].keys()
            for uo in uok:
                self.unit_obj[i][uo].kill()
                del self.unit_obj[i][uo]
            #clean images
            ik = self.display.images.keys()
            for iki in ik:
                if iki.count(",") and iki.startswith("u%i_"%i):
                    del self.display.images[iki]

    def Get_unit_object(self,po):
        #return the root object
        k = self.unit_obj[po].keys()
        if len(k) == 0:
            return None
        obj = self.Get_obj_objfromname(k[0])
        if obj:
            if obj.root:
                obj = self.Get_obj_objfromname(obj.root)
        return obj

    def Set_unit(self,po,what,shift=[0,0]):
        i,o = 1,self.display.orientation
        if po == "left":
            i,o = 0,0
        elif po == "right":
            i,o = 2,1
        self.Reset("u%i"%i)
        if what != "" and what in self.unit_modules:
            mo = self.unit_modules[what]
            u = mo(self)
            u.Load(i,o,[self.pos3[i][0]+shift[0],self.pos3[i][1]+shift[1]])

    def Set_decor(self,style):
        self.Reset("d")
        dk = self.decor_modules.keys()
        mo = None
        if type(style[1]) == type(1):
            dk.sort()
            i = style[1]%len(dk)
            mo = self.decor_modules[dk[i]]
        else:
            mo = self.decor_modules[style[1]]
        d = mo(self)
        d.Load(style[0],style[2])

    def Get_obj_varfrommod(self,mod):
        """
        Utility function for Get_obj
        Return the value from "mod"
        """
        S=mod.get("S",1)
        A=mod.get("A",0)
        I=mod.get("I",0)
        T=mod.get("T",0)
        return S,A,I,T

    def Get_obj_namefrommod(self,on,mod):
        """
        Utility function for Get_obj
        Return the name of an image for the case of an original name "on" and a modificator "mod"
        """
        S,A,I,T = self.Get_obj_varfrommod(mod)
        rS,rA,rI,rT = round(S,2),int(A%1440),(I)%2,int(T)
        nname = "%s,S%.2f,A%i,I%i,T%i"%(on,rS,rA,rI,rT)
        return nname

    def Set_motion(self,name,actions):
        if name in self.motions.keys():
            print "Warning: there is already a motion with name %s, and it will be overwritten"%name
        self.motions[name] = Motion(self,actions)
        self.motions[name].time_shift = -1*self.motion_timer

    def Get_obj_objfromname(self,obj_name):
        obj = None
        if obj_name.startswith("d_"):
            obj = self.decor_obj.get(obj_name,None)
        elif obj_name.startswith("u0_"):
            obj = self.unit_obj[0].get(obj_name,None)
        elif obj_name.startswith("u1_"):
            obj = self.unit_obj[1].get(obj_name,None)
        elif obj_name.startswith("u2_"):
            obj = self.unit_obj[2].get(obj_name,None)
        else:
            print "Warning: object name does not follow convention",obj_name
        return obj

    def Set_object_quick(self,name,image_name,screen_pos,anchor_pos):
        if not name.startswith("d_"):
            print "Warning: please start decor objects name with d_"
        if name in self.decor_obj:
            print "Warning: two times the same name for",name
        self.decor_obj[name] = Object(self,name,image_name,anchor_pos,screen_pos)

    def Set_object(self,name,or_image,screen_pos,anchor_pos,mod):
        """
        Create a new object, with the name "name", based on the image "or_image"
        this image is modified according to "mod"
        the anchor of the object is at position "anchor_pos"
        and the object is put in position "screen_pos"
        """
        image_name = self.Get_obj_namefrommod(or_image,mod)
        if name.startswith("u1_"):
            if name in self.unit_obj[1]:
                print "Warning: two times the same name for",name
            self.unit_obj[1][name] = ObjectU(self,name,image_name,anchor_pos,screen_pos)
        elif name.startswith("u0_"):
            if name in self.unit_obj[0]:
                print "Warning: two times the same name for",name
            self.unit_obj[0][name] = ObjectU(self,name,image_name,anchor_pos,screen_pos)
        elif name.startswith("u2_"):
            if name in self.unit_obj[2]:
                print "Warning: two times the same name for",name
            self.unit_obj[2][name] = ObjectU(self,name,image_name,anchor_pos,screen_pos)
        elif name.startswith("d_"):
            if name in self.decor_obj:
                print "Warning: two times the same name for",name
            self.decor_obj[name] = Object(self,name,image_name,anchor_pos,screen_pos)
        else:
            print "Warning: please start object name according to convention (u0_,u1_,u2_,d_)",name

    def Set_containers(self,sdecor,sunit):
        Object.containers = sdecor
        ObjectU.containers = sunit



            




