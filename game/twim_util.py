#    This file is part of TWIM.
#
#    TWIM is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    TWIM is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with TWIM.  If not, see <http://www.gnu.org/licenses/>.

"""
Some usefull functions used in both twim_server and twim_client
"""

import os
import imp

VERSION="0.1.161117"
VERSION_NAME="Fluffy rabbit"

def Get_savedir():
    """
    Return (or create) the dictionnaries where the files are saved
    """
    
    #1) check on all those directories for a 'save' directory containing a '.twim_save' file
    #priority to the last on the list
    all_d = []
    all_d.append( os.path.join(os.environ["HOME"],".Twim") )
    all_d.append( os.path.join(os.environ["HOME"],".local/share/Twim") )
    all_d.append( os.path.join(os.environ["HOME"],".local/share/twim") )
    all_d.append( os.getcwd() )
    all_d.append( os.environ.get("TWIM_DIR","") )

    found = ""
    for d in all_d:
        if d and os.path.isdir(d) and os.path.isdir(os.path.join(d,"save")):
            wdd = os.path.join(d,"save")
            ad = [ f for f in os.listdir(wdd) if f.endswith(".twim_save")]
            if ad:
                if found:
                    print "There are several places with saved files:"
                    print found
                    print wdd
                found = wdd

    #2) if not found, use ".local/share/twim/", and create it
    if found == "":
        d1 = os.path.join(os.environ["HOME"],".local")
        d2 = os.path.join(os.environ["HOME"],".local/share")
        d3 = os.path.join(os.environ["HOME"],".local/share/twim")
        found = os.path.join(os.environ["HOME"],".local/share/twim/save")
        if not os.path.exists(d1):
            os.mkdir(d1)
        if not os.path.exists(d2):
            os.mkdir(d2)
        if not os.path.exists(d3):
            os.mkdir(d3)
        if not os.path.exists(found):
            os.mkdir(found)

    #3) return the value
    return found


def Get_ressources():
    """
    Return the dictionnaries containing the units and the decors
    """
    
    #1) create the list of directories where to check
    #the first occurence has priority: if a same object is found in another directory, it is NOT loaded

    all_d = []
    all_d.append( os.environ.get("TWIM_DIR","") )
    all_d.append( os.path.join(os.environ["HOME"],".local/share/twim") )
    all_d.append( os.path.join(os.environ["HOME"],".local/share/Twim") )
    all_d.append( os.path.join(os.environ["HOME"],".Twim") )
    all_d.append( "/usr/share/twim" )
    all_d.append( "/usr/share/Twim" )
    all_d.append( "/usr/local/share/twim" )
    all_d.append( "/usr/local/share/Twim" )
    all_d.append( "/usr/local/games/twim" )
    all_d.append( "/usr/local/games/Twim" )
    all_d.append( "/usr/games/twim" )
    all_d.append( "/usr/games/Twim" )
    all_d.append( "/usr/share/games/twim" )
    all_d.append( "/usr/share/games/Twim" )
    all_d.append( os.getcwd() )
    directories = []
    for d in all_d:
        if d and d not in directories and os.path.isdir(d):
            directories.append(d)

    #2) complete the directories with all subdir
    # if there is a virtual link making a loop, this will get stuck
    while 1:
        nd = []
        changed = 0
        for wdd in directories:
            nd.append(wdd)
            ad = [ f for f in os.listdir(wdd) if os.path.isdir(os.path.join(wdd,f))]
            ad.sort()
            for f in ad:
                d = os.path.join(wdd,f)
                if d not in directories:
                    changed = 1
                    nd.append( d )
        if changed:
            directories = nd[:]
        else:
            break

    #3) find the ressources
    #convention:
    # we don't care about the path
    # 1 object per .py files
    # decors are in decor_*.py files
    # as Decor class
    # units are in unit_*.py files
    # as Unit class
    # the name of the thing is given inside the class
    # good practice: use the same name as file and class (unit_X.py is named X)
    #if something already named like that is found, it is NOT loaded

    modules_unit = {}
    modules_decor = {}

    #you can veto a directory by creating a file named "veto" in

    for wdd in directories:
        l_unit = [ f for f in os.listdir(wdd) if os.path.isfile(os.path.join(wdd,f)) and f.startswith("unit_") and f.endswith(".py")]
        l_decor = [ f for f in os.listdir(wdd) if os.path.isfile(os.path.join(wdd,f)) and f.startswith("decor_") and f.endswith(".py")]
        l_flag = [ f for f in os.listdir(wdd) if os.path.isfile(os.path.join(wdd,f)) and f == "veto"]
        if l_flag:
            continue
        for i_u in l_unit:
            module = imp.load_source(i_u[:-3],os.path.join(wdd,i_u))
            module.Unit.PATH = wdd
            name = module.Unit.NAME
            if name not in modules_unit:
                modules_unit[name] = module.Unit
            else:
                print "Unit",name,"already loaded"
                print " loaded from",modules_unit[name].PATH
                print " duplicated in",wdd
        for i_d in l_decor:
            module = imp.load_source(i_d[:-3],os.path.join(wdd,i_d))
            module.Decor.PATH = wdd
            name = module.Decor.NAME
            if name not in modules_decor:
                modules_decor[name] = module.Decor
            else:
                print "Decor",name,"already loaded"
                print " loaded from",modules_decor[name].PATH
                print " duplicated in",wdd

    return modules_unit,modules_decor


