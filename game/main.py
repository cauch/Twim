#!/usr/bin/env python

#    This file is part of TWIM.
#
#    TWIM is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    TWIM is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with TWIM.  If not, see <http://www.gnu.org/licenses/>.

#first, display an interface to choose what we want to do
#but I will do this later
#let say: play a game locally with AI

import subprocess

import sys

add = ""
if "--load" in sys.argv:
    add = "--load "+sys.argv[sys.argv.index("--load")+1]
r1 = subprocess.call('python ./twim_network.py IGServer '+add+' &', shell=True)
import time
time.sleep(1)
r2 = subprocess.call('python ./twim_network.py IGClient',   shell=True)
#r3 = subprocess.call('python ./twim_network.py AIClient',   shell=True)

