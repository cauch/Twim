#    This file is part of TWIM.
#
#    TWIM is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    TWIM is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with TWIM.  If not, see <http://www.gnu.org/licenses/>.

"""
Classes for the spell (because it is a big one)
used by:
twim_server
"""

import random
from math import *
    
class Spell:
    def __init__(self,n,w):
        """
        Spell

        argument:
           n: the name value
        """
        self.world = w
        self.name = n    #name of the spell
        self.what = 0    #type of the spell
        self.val1 = [0,0,0,0,0,0]
        self.val2 = [0,0,0,0,0,0]

        self.location = []
        #used for spawing random spells in the world
        #rooms have colors
        #for a given room, take a random color of the room
        #and take a random location value,
        #and if it matches, spawn the spells in this room
        #if empty, no spawn allowed
        #if -1, allowed
        #example: [-1,0,0,2,2,2,2,2,2,3]: 30% proba of being accepted in 0, 70% proba in 2, 20% proba in 3, 10% proba in others 
        
        self.level = -1
        #if the spell is powerfull, this value is high

        self.delay = 0
        #when the spell is used, you have to wait this amount of second before being able to use it again

        #type can be:
        #1: the command to get the description of the other spells
        #2: the command to move
        #3: move value from S to H (defensive)
        #4: move value from H to S (retreat)
        #5: attack (use H to the ennemy) (attack)
        #6: create value of a given color for the cost of other (create)
        #7: convert (for the following spell, value of color i are converted into color given in val1[i]) (convert)

        self.used = 0
        #to avoid always finding the same spells

        self.current_modifier = {}

    def __getstate__(self):
        """
        Veto attributes that should not be saved in files
        """
        pickle_veto = ["world",
                       ]
        return {k: v for k, v in self.__dict__.iteritems() if k not in pickle_veto}

    def Get_val(self,i):
        v = []
        if i == 1:
            v = self.val1[:]
        elif i == 2:
            v = self.val2[:]
        v = self.Apply_modifier(v,self.current_modifier)
        return v

    def Apply_modifier(self,v,m):
        if "convert" in m:
            v2 = [0,0,0,0,0,0]
            c = m["convert"]
            for i in range(len(c)):
                #the color i should be considered as the color c[i]
                v2[c[i]] += v[i]
            v = v2
        if "multiplier" in m:
            mu = m["multiplier"]
            v = [x*mu for x in v]
        return v

    def Get_val1(self):
        return self.Get_val(1)

    def Get_val2(self):
        return self.Get_val(2)

    def Randomize(self,need=""):
        """
        Create a random spell
        """
        if need:
            self.what = int(need.split("_")[0])
            self.level = int(need.split("_")[1])
            if need.split("_")[2]:
                for s in need.split("_")[2]:
                    self.location.append(int(s))

        #random type
        if self.what == 0:
            self.what = random.randint(3,7)
        #random color
        while self.location == []:
            self.location = [0,0,0,0,1,1,1,2,2,3,4,5]
            random.shuffle(self.location)
            if random.randint(0,2) == 0:
                self.location = self.location[:3]
            elif random.randint(0,1) == 0:
                self.location = self.location[:2]
            else:
                self.location = self.location[:1]
            for i in range( 2+(2*len(self.location)) ):
                if random.randint(0,1):
                    self.location.append(self.location[i%len(self.location)])
            self.location.sort()
            if self.location.count(self.location[0]) == len(self.location):
                self.location = self.location[:1]
            if self.what == 6:
                if len([i for i in range(len(self.location)) if i == self.location.index(self.location[i])]) > 3:
                    self.location = []
        #random level
        if self.level == -1:
            self.level = 10-int( 11./9 * ( sqrt(random.randint(1,99))-1 ) )

        if self.what == 3:
            self.Randomize_spell_basic()
        elif self.what == 4:
            self.Randomize_spell_basic()
            self.val1 = [int(x*(random.random()+3.1)) for x in self.val1]
            #val1 is: what I would like to remove from H
            #val2 is: what is the commission on what I remove
            # so, if H is >= val1, I remove val1 and I gain val1-val2
            # if H < val1, I remove H and I gain H-val2
        elif self.what == 5:
            self.Randomize_spell_basic()
            self.val2 = [(x+4)*min(x,1) for x in self.val2]
            self.val2 = [int(x*(1.2+(0.8*random.random()) )) for x in self.val2]
        elif self.what == 6:
            self.Randomize_spell_basic()
            while [self.val1[i]*self.val2[i] for i in range(6)] != [0,0,0,0,0,0]:
                #there is always a combination, because len()<=3
                random.shuffle(self.val2)
        elif self.what == 7:
            self.Randomize_spell_convert()


    def Randomize_spell_convert(self):

        rc = 2
        if self.level > 3:
            rc += 1
        if self.level > 5:
            rc += 1
        if self.level > 7:
            rc += 1

        cl = [0,1,2,3,4,5]

        if random.randint(0,1):
            n_c = random.randint(1,3)
        else:
            n_c = 1
        te = 0
        while te >= 0 and n_c > 0:
            i1 = random.randint(0,5)
            i2 = random.randint(0,5)
            if i1 == i2:
                continue
            if cl[i1] != i1:
                continue
            te += i2-i1
            te += i2
            n_c += -1
            cl[i1] = i2

        te = 10+(te/3)-self.level
        #the more te is high, the more difficult it should be
        
        n_co = 1
        n_de = 0
        for i in range(te):
            if random.randint(0,1):
                if random.randint(0,1):
                    n_co += 1
                else:
                    n_de += 1

        co = [0,0,0,0,0,0]
        while n_co > 0:
            done = -1
            while done == -1:
                i = random.randint(0,5)
                if cl[i] != i:
                    co[i] += 1
                    done = i+1
            n_co += -0.333*done

        self.val1 = cl[:]
        self.val2 = co[:]
        self.delay = 0#n_de*10
        self.val1 = [1,1,2,3,4,5]
        self.val2 = [1,0,0,0,0,0]
        #print self.val1,self.val2,self.delay,self.level

    def Randomize_spell_basic(self):
        #fill val2 and val1 based on colors
        #add more to val1 based on the level

        total = 1
        delay = 0
        addco = 0
        
        addco = 10-self.level
        ac = addco
        for i in range(ac):
            if random.randint(0,1):
                continue
            delay += 1
            addco += -1
        for i in range(self.level):
            if random.randint(0,1):
                continue            
            total += 1
        if total >= 5:
            total = 5
        addco = int(addco*0.5)

        loc = self.location
        for i in range(total):
            random.shuffle(loc)
            self.val1[loc[0]] += 1
            self.val2[loc[0]] += 1
        for i in range(addco):
            random.shuffle(loc)
            self.val1[loc[0]] += 1

        self.delay = 0#delay*10


    def Test_color(self,cl):
        """
        Test is the spell is suited for the given color

        arguments:
           cl: the color, -1 if it does not matter

        return:
           0: suited
           1: not suited
           2: not suited but could be 
        """
        if cl == -1:
            return 0
        else:
            if cl not in self.location:
                return 1
            if cl == self.location[random.randint(0,len(self.location)-1)]:
                return 0
            return 2

    def Try_to_change_status(self,unit_involved,unit_name,valH,valS):
        """
        Look inside unit_involved[unit_name] and try to apply the sum of valH, valS

        arguments:
           unit_involved: the dico of the units, containing dico of "statusH" and "statusS"
           unit_name: the unit that we want to test
           valH, valS: the vectors to sum

        return:
           vector of 2 vectors:
            1st= new valH
            2nd= new valS
            [[-99],[-99]] if failed somehow
        """

        if unit_name not in unit_involved:
            return [[-99],[-99]]

        clH = unit_involved[unit_name]["statusH"]
        clS = unit_involved[unit_name]["statusS"]

        for i in range(6):
            clH[i] += valH[i]
            clS[i] += valS[i]

        return [clH,clS]


    def Play(self,arg,player_name,unit_involved,modifier):
        """
        Execute the spell
        For clearer display, every type has it's own function

        arguments:
           arg: text that follows the command
           player_name: the current player_name
           unit_involved: statusH and statusS of each unit involved, that will be applied if the spell is successful
           modifier: how the costs are modified

        return:
           text that will be sent to the client
           (without the initial "COok")
           success number: 0 if not applied, 1 if applied, -1 if typo, 2 if applied but no delay
        """
        #1) check the delays:
        if self.name in self.world.units[player_name].spell_delays:
            time_now = self.world.server.time
            time_back = self.world.units[player_name].spell_delays[self.name]
            if time_back >= time_now:
                #still in colddown
                return "",0
            else:
                del self.world.units[player_name].spell_delays[self.name]
        #2) 
        self.current_modifier = modifier
        rv,su = "",0
        if self.what == 1:
            rv,su = self.Play_1(arg,player_name,unit_involved)
        elif self.what == 2:
            rv,su = self.Play_2(arg,player_name,unit_involved)
        elif self.what == 3:
            rv,su = self.Play_3(arg,player_name,unit_involved)
        elif self.what == 4:
            rv,su = self.Play_4(arg,player_name,unit_involved)
        elif self.what == 5:
            rv,su = self.Play_5(arg,player_name,unit_involved)
        elif self.what == 6:
            rv,su = self.Play_6(arg,player_name,unit_involved)
        elif self.what == 7:
            rv,su = self.Play_7(arg,player_name,unit_involved)

        if su == 1 and self.delay:
            self.world.units[player_name].spell_delays[self.name] = self.world.server.time+self.delay
            self.world.server.Notify_modification_player(player_name)

        modifier = self.current_modifier

        return rv,su

    def Play_applyandnotify(self,unit_involved,player_name,v):
        """
        Some things that are usually done at the end of a lot of spell
        """
        unit_involved[player_name]["statusH"] = v[0][:]
        unit_involved[player_name]["statusS"] = v[1][:]

        #notify room
        n = self.world.units[player_name].room_keyname
        unit_involved[n] = []

    def Play_1(self,arg,player_name,unit_involved):
        """
        Execute the spell of type 1 (description)

        arguments:
           arg: text that follows the command
           player_name: the name of the player that emitted the spell
           unit_involved: statusH and statusS of each unit involved, that will be applied if the spell is successful
           modifier: how the costs are modified

        return:
           text that will be sent to the client
           (without the initial "COok")
           success number: 0 if not applied, 1 if applied, -1 if typo, 2 if applied but no delay
        """
        nextspell = self.name
        if arg:
            nextspell = arg[0]
        if nextspell in self.world.spells.keys():
            #self.world.spells[nextspell].current_player = player_name
            dico = self.world.spells[nextspell].Get_description()
            if self.current_modifier:
                dico["modifier"] = self.current_modifier
            return "!T"+repr(dico),1
        return "",-1

    def Play_2(self,arg,player_name,unit_involved):
        """
        Execute the spell of type 2 (move)

        arguments:
           arg: text that follows the command
           player_name: the name of the player that emitted the spell
           unit_involved: statusH and statusS of each unit involved, that will be applied if the spell is successful
           modifier: how the costs are modified

        return:
           text (repr(dico)) that will be sent to the client
           (without the initial "COok")
           success number: 0 if not applied, 1 if applied, -1 if typo, 2 if applied but no delay
        """

        d = {}
        d["type"] = "environ"


        n = "%i_%i"%(self.world.units[player_name].x,self.world.units[player_name].y)
        if n not in self.world.rooms.keys():
            print "uh?1"
        if self.world.rooms[n] == None:
            print "uh?2"

        nr = []
        for i in range(4):
            if i in self.world.rooms[n].exits:
                r = self.world.Get_nextroom(n,i)
                if r != None:
                    nr.append([i,r])

        if arg:
            if arg[0] not in self.world.irooms.keys():
                print "uh?3"
            else:
                n = self.world.irooms[arg[0]]
                if n not in self.world.rooms.keys():
                    print "uh?4"
                    return "",-1
                if self.world.rooms[n] == None:
                    print "uh?5"
                    return "",-1
                #check the room is next
                if arg[0] not in [x[1].name for x in nr]:
                    print "uh?6"
                    return "",-1
                cl2p = self.world.rooms[n].cl
                cl2p = self.Apply_modifier(cl2p[:],self.current_modifier)
                clpl = self.world.units[player_name].statusH
                ncl = [clpl[i]-cl2p[i] for i in range(6)]
                if min(ncl) < 0:
                    return "",0
                self.world.server.Move_player(player_name,n)
                unit_involved[player_name]["statusH"] = ncl[:]                        
                n0 = self.world.units[player_name].room_keyname
                unit_involved[n] = []
                unit_involved[n0] = []
                return "",1
        else:
            d["here"] = self.world.rooms[n].name
            it = ["north","east","south","west"]
            for i,r in nr:
                d[it[i]+"_name"] = r.name
                d[it[i]+"_cl"] = r.cl[:]
            return "!T"+repr(d),2
        return "",-1

    def Play_3(self,arg,player_name,unit_involved):
        """
        Execute the spell of type 3 (defensive)
        """
        v = self.Try_to_change_status(unit_involved,player_name,self.Get_val2()[:],[-1*x for x in self.Get_val1()])
        if min(v[0]+v[1])<0:
            return "",0
        self.Play_applyandnotify(unit_involved,player_name,v)
        return "",1

    def Play_4(self,arg,player_name,unit_involved):
        """
        Execute the spell of type 4 (retreat)
        """
        ov = [ unit_involved[player_name]["statusH"][:], unit_involved[player_name]["statusS"][:] ]
    
        clH = unit_involved[player_name]["statusH"]
        clS = unit_involved[player_name]["statusS"]

        #val1 is: what I would like to remove
        #val2 is: what is the commission on what I remove
        # so, if H is >= val1, I remove val1 and I gain val1-val2
        # if H < val1, I remove H and I gain H-val2
        v1 = self.Get_val1()[:]
        v2 = self.Get_val2()[:]

        rr = [0,0,0,0,0,0]
        gr = [0,0,0,0,0,0]
        for i in range(6):
            if clH[i]-v1[i] > 0:
                rr[i] = -1*v1[i]
            else:
                rr[i] = -1*clH[i]
            gr[i] = (-1*rr[i])-v2[i]
            if gr[i] < 0: gr[i] = 0

        v = self.Try_to_change_status(unit_involved,player_name,rr,gr)
        if len(v[0]) != 6:
            return "",0
        if min(v[0]+v[1])<0:
            return "",0
        if v == ov:
            return "",0
        self.Play_applyandnotify(unit_involved,player_name,v)
        return "",1

    def Play_5(self,arg,player_name,unit_involved):
        """
        Execute the spell of type 5 (attack)
        """
        
        if not arg:
            return "",0

        #1) identify the ennemy
        en = arg[0]
        if en not in self.world.units.keys():
            print "uh?1"
            print en
            print self.world.units.keys()
            return "",-1
        rkn = self.world.units[player_name].room_keyname
        ro = self.world.rooms[rkn]
        ip = ro.units.index(player_name)
        ie = 99
        if en in ro.units:
            ie = ro.units.index(en)
        if abs(ip-ie) != 1:
            print "uh?2",ie,en,ro.units,player_name
            return "",-1
        if en not in unit_involved:
            unit_involved[en] = {"statusS":self.world.units[en].statusS[:],"statusH":self.world.units[en].statusH[:]}

        #2) try to pay
        v = self.Try_to_change_status(unit_involved,player_name,[0,0,0,0,0,0],[-1*x for x in self.Get_val1()])
        if min(v[0]+v[1])<0:
            return "",0
        veo = unit_involved[en]["statusH"][:]
        va = [-1*x for x in self.Get_val2()]
        for i in range(6):
            if veo[i] == 0: va[i] = 0        
        ve = self.Try_to_change_status(unit_involved,en,va,[0,0,0,0,0,0])
        if [veo[i] for i in range(6) if ve[0][i]<=0 and veo[i]>0]:
            #it has been killed
            #the reward you get is: the remaining points of the opponent * ~2
            #                       + 10 points in each color of the reamining points of the opponent
            #                       + 10 points in each color of your hand
            print "has been killed"
            poto = [max(0,ve[0][j]+ve[1][j]) for j in range(6)]
            ttgS = [int ( poto[j]*( 2+random.random() ) ) for j in range(6)]
            ttgS = [ ttgS[j]+(min(poto[j],1)*10) for j in range(6)]
            ttgH = [ min(v[0][j],1)*10 for j in range(6)]
            print "has been killed",ttgS,ttgH
            unit_involved[player_name]["statusH"] = v[0][:]
            unit_involved[player_name]["statusS"] = v[1][:]
            unit_involved[en]["killed"] = 1
            v = self.Try_to_change_status(unit_involved,player_name,ttgH,ttgS)
            ve = [[0,0,0,0,0,0],[0,0,0,0,0,0]]
        self.Play_applyandnotify(unit_involved,player_name,v)
        self.Play_applyandnotify(unit_involved,en,ve)
        #it's important to notice the attacked unit that it has been attacked
        if not unit_involved[en].get("knowledge"):
            unit_involved[en]["knowledge"] = []
        unit_involved[en]["knowledge"].append("attacked %s"%player_name)
        return "",1

    def Play_6(self,arg,player_name,unit_involved):
        """
        Execute the spell of type 6 (creation)
        """
        val = [self.Get_val2()[i]-self.Get_val1()[i] for i in range(6)]
        v = self.Try_to_change_status(unit_involved,player_name,[0,0,0,0,0,0],val)
        if min(v[1])<0:
            return "",0
        self.Play_applyandnotify(unit_involved,player_name,v)
        return "",1

    def Play_7(self,arg,player_name,unit_involved):
        """
        Execute the spell of type 7 (convert)
        """
        if not arg:
            return "",0

        com = arg[0]
        ar = arg[1:]

        if com not in self.world.spells.keys():
            return "",0

        v = self.Try_to_change_status(unit_involved,player_name,[0,0,0,0,0,0],[-1*i for i in self.Get_val2()])
        if min(v[0]+v[1]) < 0:
            return "",0

        self.Add_modifier("convert",self.Get_val1())
        modifier = self.current_modifier
        r1,r2 = self.world.spells[com].Play(ar,player_name,unit_involved,modifier)
        return r1,r2

    def Add_modifier(self,what,val):
        if what == "convert":
            if "convert" in self.current_modifier:
                for i in range(6):
                    if val[i] != i:
                        j = val[i]
                        #i should be considered as j
                        for ii in range(6):
                            if self.current_modifier[ii] == i:
                                self.current_modifier[ii] = j
            else:
                self.current_modifier["convert"] = val[:]
        if what == "multiplier":
            if "multiplier" not in self.current_modifier:
                self.current_modifier["multiplier"] = 1
            self.current_modifier["multiplier"] *= val

    def Get_description(self):
        """
        Return dico with all the info needed for the description of the spell
        """
        d = {}
        d["type"] = "spell"
        d["name"] = self.name
        d["what"] = self.what
        d["val1"] = self.Get_val1()
        d["val2"] = self.Get_val2()
        d["delay"] = self.delay
        return d

####################"

