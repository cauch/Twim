# -*- coding: utf-8 -*-

#    This file is part of TWIM.
#
#    TWIM is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    TWIM is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with TWIM.  If not, see <http://www.gnu.org/licenses/>.

import os
from math import *
import string
from random import *

#import basic pygame modules
import pygame
from pygame.locals import *

from twim_object import *

class Chrono(pygame.sprite.Sprite):
    def __init__(self,d):
        """
        Sprite corresponding to the list of chronometers

        arguments:
        d: the display class
        """
        pygame.sprite.Sprite.__init__(self, self.containers)
        self.display = d
        self.w = d.dim_width*d.text_length
        self.h = d.dim_toth*d.text_height
        self.rect = pygame.Rect(0,0,0,0)
        self.image = pygame.Surface((20,self.h-d.text_height),flags=SRCALPHA).convert_alpha()
        self.pos = [self.w-20,d.text_height+5]
        #self.pos = [300,100]
        self.rect = self.image.get_rect()
        self.rect.left = self.pos[0]
        self.rect.top = self.pos[1]
        self.chronos = []
        self.names = []
        #if delay-self.time > 0:
        #text += "$nwill be active in %s"%self.display.Transform_time(delay-self.time,1)

    def Add(self,time, name):
        if time > self.display.client.time:
            if name not in self.names:
                self.chronos.append([self.display.client.time,time])
                self.names.append(name)
            else:
                i = self.names.index(name)
                if len(self.chronos[i]) == 2:
                    self.chronos[i][1] = time
                else:
                    self.chronos[i] = [self.display.client.time,time]


    def Refresh(self):
        nc = []
        nn = []
        self.image = pygame.Surface((20,self.h-self.display.text_height),flags=SRCALPHA).convert_alpha()
        current_time = self.display.client.time
        cy = 0
        for i in range(len(self.chronos)):
            if len(self.chronos[i]) == 2:
                et1,et2 = self.chronos[i][0],self.chronos[i][1]
                if et2 > current_time:
                    nc.append(self.chronos[i])
                    nn.append(self.names[i])
                    percent = 1.-( (current_time-et1)*1./(et2-et1) )
                    fpi = -1*i
                    if percent > 0:
                        self.Pie([0,cy],percent,fpi)
                    cy += 20
                else:
                    nc.append([20])
                    nn.append(self.names[i])
            else:
                ci = self.chronos[i][0]
                if ci > 0:
                    cy += ci
                    nc.append([ci-2])
                    nn.append(self.names[i])
        self.chronos = nc
        self.names = nn

    def Pie(self,pos,percent,fpi):
        
        fa = self.display.text_flickering_amp
        ff = self.display.text_flickering_per
        level = self.display.text_bkgd_alpha
        tf_tim = self.display.text_flickering_pattern[fpi][0]
        tf_dev = self.display.text_flickering_pattern[fpi][1]
        tf_tim += -1

        change = 0
        if tf_tim <= 0:
            change = 1
            tf_tim = ff+randint(0,ff)+randint(0,ff)+randint(0,ff)
            tf_dev = randint(0,fa)

        self.display.text_flickering_pattern[fpi][0] = tf_tim

        if change:
            self.display.text_flickering_pattern[fpi][1] = tf_dev
        level = level-(2*tf_dev)
        
        angle = int(percent*360)
        ns = pygame.Surface((200,200),flags=SRCALPHA).convert_alpha()
        cx,cy = 100,100
        r = 100
        p = [(cx, cy)]
        for n in range(0,angle):
            x = cx-int(r*sin(n*pi/180))
            y = cy-int(r*cos(n*pi/180))
            p.append((x, y))
        p.append((cx, cy))

        if len(p) > 2:
            pygame.draw.polygon(ns, (255, 255, 255, level), p)
            ns2 = pygame.transform.smoothscale(ns, (16,16))
            self.image.blit(ns2,(2+pos[0],2+pos[1]))


class Curtain(pygame.sprite.Sprite):
    def __init__(self,d):
        pygame.sprite.Sprite.__init__(self, self.containers)
        self.display = d
        w = d.text_length*d.dim_width
        h = d.text_height*d.dim_toth
        self.rect = pygame.Rect(0,0,w,h)
        self.image = pygame.Surface((w,h)).convert_alpha()
        self.image.fill((0,0,0,255))
        self.level = -1

    def Refresh(self):
        #if self.display.clock.get_time() < 5:
        #    self.level = -1
        #    self.image.fill((0,0,0,0))
        #    self.display.anim_actions["curtain"] = 0
        #    return
        if self.level == -1:
            self.display.anim_actions["curtain"] = 0
            return
        
        self.level += 1
        if self.level > self.display.anim_curtain:
            self.level = -1
            self.display.anim_actions["curtain"] = 0
            return

        mid = (self.display.anim_curtain-1)/2
        transp_step = int(255/mid)
        transparency = 255 
        if self.level < mid:
            transparency = int(transp_step*self.level)
            self.display.anim_actions["curtain"] = 1
        elif self.level == mid:
            transparency = 255 
            self.display.anim_actions["curtain"] = 2
            self.display.Set_decor()
        else:
            transparency = int(255+(transp_step*(mid-self.level)))
            self.display.anim_actions["curtain"] = 3
        if transparency < 0:
            transparency = 0
        self.image.fill((0,0,0,transparency))
        

class Letter(pygame.sprite.Sprite):
    def __init__(self,d,letter,pos):
        """
        Sprite corresponding to 1 letter

        arguments:
        d: the display class
        letter: the string corresponding to the image (ex.: "l_d" for the letter "d")
        pos: the position in pixel
        """
        pygame.sprite.Sprite.__init__(self, self.containers)
        self.display = d
        self.rect = pygame.Rect(0,0,0,0)
        #1) check that the letter exists
        if "l_"+letter not in self.display.images.keys():
            print "Error in Letter:",letter
        self.letter = "l_"+letter
        self.image = self.display.images["l_"+letter]
        self.fron = self.display.images["l_"+letter]
        self.bkgd  = None
        self.bkgd_bl = 0
        self.pos = pos
        self.rect = self.image.get_rect()
        self.style = {"cl":[255,255,255],"bl":0}
        self.dying = -1
        self.dead = 0

    def Start_dying(self,i=10):
        """
        Start the animation of disappearing
        """
        self.dying = i

    def Refresh(self):
        """
        Refresh the image (when moved, but also for the blinking animation)
        """
        self.rect.left = self.pos[0]
        self.rect.top = self.pos[1]

        cl = self.style["cl"]
        bl = self.style["bl"]

        w,h = self.image.get_width(),self.image.get_height()

        fa = self.display.text_flickering_amp
        ff = self.display.text_flickering_per

        fpi = (self.pos[0]/self.display.text_length)+((self.pos[1]/self.display.text_height)*self.display.dim_width)
        
        level = self.display.text_bkgd_alpha
        tf_tim = self.display.text_flickering_pattern[fpi][0]
        tf_dev = self.display.text_flickering_pattern[fpi][1]
        tf_tim += -1

        change = 0
        if tf_tim <= 0:
            change = 1
            tf_tim = ff+randint(0,ff)+randint(0,ff)+randint(0,ff)
            tf_dev = randint(0,fa)

        self.display.text_flickering_pattern[fpi][0] = tf_tim

        if change or self.bkgd == None:
            self.display.text_flickering_pattern[fpi][1] = tf_dev
            level = level-(2*tf_dev)
            self.bkgd = pygame.Surface((w,h)).convert_alpha()
            self.bkgd.fill((cl[0],cl[1],cl[2],level))
            change = 1

        if bl != 0:
            self.bkgd_bl += 1
            level = self.display.text_bkgd_alpha
            self.bkgd.fill((cl[0],cl[1],cl[2],level))
            if self.bkgd_bl > bl:
                self.bkgd.fill((cl[0],cl[1],cl[2],0))
            if self.bkgd_bl > 2*bl:
                self.bkgd_bl = 0
            change = 1

        if change:
            self.image = pygame.Surface((w,h))
            self.image.blit(self.bkgd,(0,0))
            self.image.blit(self.fron,(0,0))
        
        if self.dying >= 0:
            self.image.set_alpha(25*self.dying)
            self.dying += -1
            self.rect.top += -1*((9-self.dying)**1)
            if self.dying == 0:
                self.dead = 1
                self.kill()




class Text:
    def __init__(self,d,what,pos,text):
        """
        Class to deal with several letter
        The difficulty is the blinking: you don't want to reset everything for the letters that did not change

        arguments:
        d: the display class
        what: how to treat the text argument and the refreshing, can be "menu", "entry", "status", "text"
        pos: the position in letter size
        text: the text (can be string, can be other, based on what
        """
        self.display = d
        self.text = text
        self.pos = pos
        self.what = what
        self.letters = []
        self.dying_letters = []
        self.dead = 0
        self.Init()

    def Init(self):
        """
        Create for the first time
        """
        x = 1+(self.pos[0]*self.display.text_length)
        y = 1+(self.pos[1]*self.display.text_height)
        if self.what == "menu" or self.what == "entry":
            #simple text, with blinking cursor for "entry"
            #text = "blahblah"
            for t in self.text:
                l = Letter(self.display,t,[x,y])
                l.Refresh()
                self.letters.append(l)
                x += self.display.text_length
            if self.what == "entry":
                l = Letter(self.display," ",[x,y])
                l.style["cl"] = [40,40,40]
                l.style["bl"] = 15
                l.Refresh()
                self.letters.append(l)
        elif self.what == "status":
            #display the values of the colors
            #text = [1,10,0,45,0,2]
            for i,v in enumerate(self.text):
                if v == 0:
                    continue
                vstr = str(v)
                for t in vstr:
                    l = Letter(self.display,t,[x,y])
                    l.style["cl"] = self.display.cl[i]
                    l.Refresh()
                    self.letters.append(l)
                    x += self.display.text_length
        elif self.what == "text":
            #simple text
            #text = "there is my text$nwith special formating"
            #
            #formating:
            #$n: newline
            #$1text$e: use color 1 (index 0)
            #$btext$e: use bold color (grey)
            by_lines = self.text.split("$n")
            ix = x+0
            for l in by_lines:
                color = "e"
                for t in l:
                    color,skip,letter = self.Write_withtags(t,color,[x,y])
                    if skip:
                        continue
                    letter.Refresh()
                    self.letters.append(letter)
                    x += self.display.text_length
                y += self.display.text_height
                x = ix+0

    def Write_withtags(self,t,color,letter):
        #letter is either the already existing letter
        #or the position of the new one
        if t == "$":
            color = ""
            return color,1,None
        if color == "":
            color = t
            return color,1,None
        if type(letter) == type([]):
            letter = Letter(self.display,t,letter)
        if color in "123456":
            letter.style["cl"] = self.display.cl[int(color)-1]
        if color in "b":
            letter.style["cl"] = [120,120,120]
        return color,0,letter


    def Update(self,text):
        if self.what == "status":
            ix = 0
            wtd = 0
            for i in range(6):
                wtd = 0
                if self.text[i] != text[i]:
                    wtd = 1
                for ll in range(len(str(self.text[i]))):
                    if ix+ll < len(self.letters):
                        if wtd:
                            self.letters[ix+ll].Start_dying()
                        else:
                            self.letters[ix+ll].dead = 1
                            self.letters[ix+ll].kill()
                ix += len(str(self.text[i]))
        else:
            for l in self.letters:
                l.dead = 1
                l.kill()
        self.text = text
        self.Init()

    def Start_dying(self,i=10):
        """
        To kill the text with animation
        """
        for l in self.letters:
            l.Start_dying(i)

    def Refresh(self):
        """
        To refresh all the letters
        """
        nl = []
        for l in self.letters:
            l.Refresh()
            if l.dead == 0:
                nl.append(l)
        self.letters = nl
        if nl == []:
            self.dead = 1

class Display():
    def __init__(self,client):
        """
        Main class for the display-part of the client
        It contains all the pygame related stuffs
        
        arguments:
           client: Client class
        """
        self.client = client                   # Client class

        self.status = 0                        #0: not initialized, 1: initialized, 2: end_of_game

        self.text_height = 20                  # size (pixel) of the text height
        self.text_length = 10                  # size (pixel) of the text length
        self.text_bkgd_alpha = 200             # value of the alpha for the bkgd of letters
        self.text_flickering_amp = 20          # level of flickering of the text background
        self.text_flickering_per = 20          # period of flickering of the text background
        self.text_flickering_pattern = []      # store the info of the flickering in x,y position, as a linear vector (index = y*w+x)

        self.dim_menu = 2                      # size (letter) of the menu board, at the top
        self.dim_entry = 1                     # size (letter) of the entry line, at the bottom
        self.dim_width = 80                    # size (letter): width
        self.dim_height = 19                   # size (letter): height
        self.dim_toth = self.dim_menu+self.dim_height+self.dim_entry

        w = self.dim_width*self.text_length
        h = self.dim_toth*self.text_height
        self.screenrect = Rect(0, 0, w+2, h+2) # size (pixel) of the pygame window
        self.winstyle = 0                      # style of the pygame window (0 or FULLSCREEN)
        self.screen = -1                       # the screen object

        self.background = -1                   # background surface

        self.sdecor = -1                       # sprites for the decor
        self.sunit = -1                        # sprites for the unit

        self.anim_curtain = 37                  # number of steps for the curtain transition
        self.anim_unit = 10                    # number of steps for the unit transition
        self.anim_decor = 10                   # number of steps for the decor transition

        self.anim_actions = {}                 # the current action, with the argument
        self.anim_actions["curtain"] = 0
        self.anim_actions["left"] = 0
        self.anim_actions["center"] = 0
        self.anim_actions["right"] = 0
        self.anim_actions["decor"] = 0

        self.stext = -1                        # sprites for the text
        self.text_obj = {}                     # list of text objects

        self.other_obj = {}                    # just one object, that displays the chronos of cooldown for spells

        self.images = {}                       # all the loaded images

        self.command = ""                      # command entered by the player

        self.clock = -1                        # clock of the game

        self.cl = []

        self.orientation = 0                   # orientation of the player: 0 to the left, 1 to the right

        for x in range(self.dim_width+1):
            for y in range(self.dim_toth):
                self.text_flickering_pattern.append([0,0])

        self.object_tool = Object_tool(self)


    def Init(self):
        """
        Initializes everything for the pygame environment
        """
        
        #see if we can load more than standard BMP
        if not pygame.image.get_extended():
            raise SystemExit, "Sorry, extended image module required"

        pygame.init()
        if pygame.mixer and not pygame.mixer.get_init():
            print 'Warning, no sound'
            pygame.mixer = None

        # Set the display mode
        bestdepth = pygame.display.mode_ok(self.screenrect.size, self.winstyle, 32)
        #
        #self.screen = pygame.display.set_mode(self.screenrect.size, pygame.HWSURFACE | pygame.OPENGL | pygame.DOUBLEBUF, bestdepth)

        pygame.display.gl_set_attribute(pygame.GL_DEPTH_SIZE, 16)
        pygame.display.gl_set_attribute(pygame.GL_STENCIL_SIZE, 1)
        pygame.display.gl_set_attribute(pygame.GL_ALPHA_SIZE, 8)
        self.screen = pygame.display.set_mode(self.screenrect.size, pygame.HWSURFACE | pygame.DOUBLEBUF, bestdepth)

        #load images
        self.Load_images()

        #background
        self.background = pygame.Surface(self.screenrect.size)
        self.background = self.background.convert_alpha()
        self.background.fill((0, 0, 0))
        #self.screen.blit(self.background, (0,0))
        pygame.display.flip()
                        
        self.stext = pygame.sprite.LayeredUpdates()
        self.sdecor = pygame.sprite.LayeredUpdates()
        self.sunit = pygame.sprite.LayeredUpdates()
    
        #decorate the game window
        icon = pygame.Surface((20,20))
        icon.fill(self.cl[1])
        icon2 = pygame.Surface((10,20))
        icon2.fill(self.cl[0])
        icon.blit(icon2,(0,0))
        #icon.blit(icon2,(10,0))
        #icon.blit(icon2,(5,10))
        #icon.blit(icon2,(15,10))
        icon.blit(self.images["l_t"],(5,0))
        pygame.display.set_icon(icon)
        pygame.display.set_caption('This World is Magic')
        pygame.mouse.set_visible(1)
        #todo
        #pygame.mouse.set_visible(0)

        Letter.containers = self.stext
        Chrono.containers = self.stext
        Curtain.containers = self.stext
        self.object_tool.Set_containers(self.sdecor,self.sunit)

        #the input ">"
        #self.text_obj["menu"] = Text(self,"menu",[0,0],"menu")
        self.text_obj["statusS"] = Text(self,"status",[0,0],[1,0,0,0,0,0])
        self.text_obj["statusH"] = Text(self,"status",[0,1],[1,0,0,0,0,0])
        self.other_obj["curtain"] = Curtain(self)
        self.text_obj["entry"] = Text(self,"entry",[0,self.dim_menu+self.dim_height],u"> ")
        self.text_obj["time"] = Text(self,"menu",[self.dim_width-12,0],"000/00:00:00")

        self.other_obj["chrono"] = Chrono(self)

        #clock
        self.clock = pygame.time.Clock()
        self.keyboard_skim = 0 #to reduce the speed of the key typing (which is useless)

        self.oldtime = 0

        self.status = 1

    def Set_decor(self):
        if self.client.current_room == {}:
            pass
        else:
            #self.orientation = 0
            self.object_tool.Set_decor(self.client.current_room["style"])

    def Change_decor(self):
        if self.anim_actions["curtain"] == 0:
            #start the animation
            self.other_obj["curtain"].level = 0
            self.anim_actions["curtain"] = 1
            self.Set_unit_all(1)

    def Refresh_status(self):
        statusS = self.client.current_player["statusS"]
        statusH = self.client.current_player["statusH"]
        self.text_obj["statusS"].Update(statusS)
        self.text_obj["statusH"].Update(statusH)
        if "refreshed" not in self.client.current_player.keys():
            if "what" in self.client.current_player.keys():
                self.client.current_player["refreshed"] = 1
                self.Set_player()

    def Set_unit_all(self,i):
        text = ""
        if i == 0:
            text = "left"
        elif i == 1:
            text = "center"
        elif i == 2:
            text = "right"
        if self.anim_actions[text] == 0:
            obj = self.object_tool.Get_unit_object(i)
            self.anim_actions[text] = 1
            nstep = (self.anim_curtain-1)/2# <- important, because "appear" should be called when tool_object.pos3 is correctly defined
            rt = random.randint(0,nstep/3)
            a = [{"type":"wait","time":rt},{"type":"anim","anim":"disappear","step":nstep+1-rt}]
            if i == 0:
                a.append({"type":"fct","fct":self.Set_unit_left2})
            elif i == 1:
                a.append({"type":"fct","fct":self.Set_unit_center2})
            elif i == 2:
                a.append({"type":"fct","fct":self.Set_unit_right2})
            self.object_tool.Set_motion("moveunit%sD"%text,a)
            self.object_tool.motions["moveunit%sD"%text].dont_kill = 1
            if obj:
                self.object_tool.motions["moveunit%sD"%text].list_object = [obj.object_name]

    def Set_player(self,ignoretransition=0):
        self.Set_unit_all(1)

    def Set_unit_left(self):
        self.Set_unit_all(0)

    def Set_unit_right(self):
        self.Set_unit_all(2)

    def Set_unit_all2(self,i):
        text = ""
        what = ""
        if i == 0:
            text = "left"
            what = self.client.current_unit_left["what"]
        elif i == 1:
            text = "center"
            what = self.client.current_player["what"]
            if self.status == 2:
                #if dead, we don't display him
                return
        elif i == 2:
            text = "right"
            what = self.client.current_unit_right["what"]

        self.object_tool.Set_unit(text,what,[0,300])
        self.anim_actions[text] = 0
        obj = self.object_tool.Get_unit_object(i)
        nstep = (self.anim_curtain-1)/2
        rt = random.randint(0,nstep/3)
        a = [{"type":"anim","anim":"appear","step":nstep+1-rt}]
        self.object_tool.Set_motion("moveunit%sA"%text,a)
        self.object_tool.motions["moveunit%sA"%text].dont_kill = 1
        if obj:
            self.object_tool.motions["moveunit%sA"%text].list_object = [obj.object_name]

    def Set_unit_left2(self,object_name):
        self.Set_unit_all2(0)

    def Set_unit_center2(self,object_name):
        self.Set_unit_all2(1)

    def Set_unit_right2(self,object_name):
        self.Set_unit_all2(2)

    def Load_pixmap(self,file): 
        """
        Loads an image, prepares it for play
        """
        file = os.path.join('', file)
        try:
            surface = pygame.image.load(file)
        except pygame.error:
            raise SystemExit, 'Could not load image "%s" %s'%(file, pygame.get_error())
        return surface.convert_alpha()

    def Show_killed(self,killed):

        if killed[0] == 1:
            #the player has been killed
            self.Set_unit_all(1)
            self.status = 2
            self.client.End_game()
        if "o_kc" not in self.images.keys():
            self.images["o_kc"] = pygame.Surface((20,20)).convert_alpha()
            self.images["o_kc"].fill((0, 0, 0, 0))
            pygame.draw.circle(self.images["o_kc"],(0,0,0),(10,10),10)
        
        ik = 0
        while "kc%i"%ik in self.object_tool.motions.keys():
            ik += 1
        rpos = self.object_tool.pos3[:]+[[self.object_tool.pos3[0][0]+100,self.object_tool.pos3[0][1]-100]]

        a = []
        for i in range(10):
            if i < 5:
                mod = {"T":-20,"S":1.1}
            else:
                mod = {"T":20,"S":0.75}
            a.append({"type":"mod","mod":mod})
        a.append({"type":"erase"})
        self.object_tool.Set_motion("kc%i"%ik,a)
        self.object_tool.motions["kc%i"%ik].list_object = []
        
        for ik2 in range(10):
            name = "d_kc%i_%i"%(ik,ik2)
            or_image = "o_kc"
            pos = rpos[killed[0]][:]
            pos[1] += -30
            pos[0] += random.randint(0,20)-10
            pos[1] += random.randint(0,20)-10
            anchor_pos = [10,10]
            mod = {"S":0.5,"T":100}
            self.object_tool.Set_object(name,or_image,pos,anchor_pos,mod)
            b = []
            ang = random.random()*2*pi
            for i in range(10):
                b.append({"type":"move","x,y":[int(round(cos(ang)*i)),int(round(sin(ang)*i))]})
            self.object_tool.motions["kc%i"%ik].list_object.append(name)
            self.object_tool.Set_motion("kc%ib_%i"%(ik,ik2),b)
            self.object_tool.motions["kc%ib_%i"%(ik,ik2)].list_object = [name]
        

    def Load_images(self):
        """
        Loads and creates the defaults images
        """
        self.images = {}
        
        self.images["o_empty"] = pygame.Surface((1,1)).convert_alpha()
        self.images["o_empty"].fill((0, 0, 0, 0))

        self.object_tool.Load_images()

        self.Load_letters()

    def Load_letters(self):
        lol = string.ascii_lowercase+string.digits+u"- >:/,"
        font = pygame.font.Font(pygame.font.match_font("droidsansmono"), 100)
        #font = pygame.font.Font("./Play-Regular.ttf", 100)
        for l in lol:
            textImg1 = font.render( l, 1, (0,0,0,100))
            textImg2 = font.render( l, 1, (155,155,155,100))
            w = textImg1.get_width()+5
            h = textImg1.get_height()+5
            factor = self.text_height*1./h
            bkgd = pygame.Surface((w,h)).convert_alpha()
            bkgd.fill((0,0,0,1))
            bkgd.blit( textImg2, (1,1) )
            bkgd.blit( textImg2, (3,3) )
            bkgd.blit( textImg1, (2,2) )
            bkgd2 = pygame.transform.smoothscale(bkgd, (int(w*factor), int(h*factor)))
            self.images["l_"+l] = bkgd2


    #def Colorize_surface(self, surface, red, green, blue):
    #    """
    #    Colorizes a surface
    #
    #    arguments:
    #        surface: the surface that will be colorize
    #        red, green, blue: the modification of color
    #    """
    #    
    #    arr = pygame.surfarray.pixels3d(surface)
    #    arr[:,:,0] += red
    #    arr[:,:,1] += green
    #    arr[:,:,2] += blue


    def Apply_keyboard(self,key):
        dontupdate = 0
        lovk = [K_a,K_b,K_c,K_d,K_e,K_f,K_g,K_h,K_i,K_j,K_k,K_l,K_m,K_n,K_o,K_p,K_q,K_r,K_s,K_t,K_u,K_v,K_w,K_x,K_y,K_z,
                K_0,K_1,K_2,K_3,K_4,K_5,K_6,K_7,K_8,K_9,K_KP0,K_KP1,K_KP2,K_KP3,K_KP4,K_KP5,K_KP6,K_KP7,K_KP8,K_KP9,K_MINUS,K_SPACE]
        if key in lovk:
            self.command += (string.ascii_lowercase+string.digits+string.digits+"- ")[lovk.index(key)]
        elif key == K_BACKSPACE:
            if len(self.command) >= 1:
                self.command = self.command[:-1]
        elif key == K_DELETE:
            self.command = ""
        elif key == K_UP:
            dontupdate = 1
            self.client.Move_history("up")
        elif key == K_DOWN:
            dontupdate = 1
            self.client.Move_history("down")
        elif key == K_LEFT:
            dontupdate = 1
            self.Change_player_orientation(0)
        elif key == K_RIGHT:
            dontupdate = 1
            self.Change_player_orientation(1)
        elif key == K_RETURN:
            if self.status != 2:
                self.client.Enter(self.command)
            self.command = ""
        if dontupdate == 0:
            self.text_obj["entry"].Update("> "+self.command)

    def Change_player_orientation(self,ori):
        if ori != self.orientation:
            self.orientation = ori
            obj = self.object_tool.Get_unit_object(1)
            if obj:
                obj.Apply_motion_relativemod({"I":1},{})
        #write the status of the object
        self.client.current_view = {}
        self.client.Ask_unit_description(self.orientation)
            

    def Transform_time(self,time,short=0):
        #format: "000/00:00:00"
        ntime = time
        second = ntime % 60
        ntime = (ntime - second)/60
        minute = ntime % 60
        ntime = (ntime - minute)/60
        hour = ntime % 24
        ntime = (ntime - hour)/24
        day = ntime % 1000
        tt = "%03i/%02i:%02i:%02i"%(day,hour,minute,second)
        if short:
            if day == 0:
                tt = "%02i:%02i:%02i"%(hour,minute,second)
                if hour == 0:
                    tt = "%02i:%02i"%(minute,second)
                    if minute == 0:
                        tt = "%02i"%(second)
        return tt

    def Update_view(self):

        #key and mouse event
        view_update = 0
        for event in pygame.event.get():
            #quit
            if event.type == QUIT  or (event.type == KEYDOWN and event.key == K_ESCAPE):
                self.client.playing = 0
            elif event.type == KEYDOWN:
                self.Apply_keyboard(event.key)
            elif event.type == MOUSEBUTTONDOWN:
                self.Push_down(pygame.mouse.get_pressed(),pygame.mouse.get_pos())
        keystate = pygame.key.get_pressed()
        self.keyboard_skim += 1
        if self.keyboard_skim >= 2: self.keyboard_skim = 0

        self.stext.clear(self.screen, self.background)

        #update the time
        if self.client.time != self.oldtime:
            self.oldtime = self.client.time
            self.text_obj["time"].Update(self.Transform_time(self.client.time))
            if "SendTest" in os.environ.get("TWIM_DEBUG",""):
                v = int(os.environ.get("TWIM_DEBUG","").split("SendTest")[1][:3])
                if v != 0:
                    if self.client.time%v == 1:
                        self.client.Enter("test")
                        

        #text things
        if self.text_obj:
            nl = {}
            for to in self.text_obj:
                self.text_obj[to].Refresh()
                if self.text_obj[to].dead == 0:
                    nl[to] = self.text_obj[to]
            self.text_obj = nl

        #chrono, curtain
        for k in self.other_obj:
            self.other_obj[k].Refresh()

        #object_tool
        self.object_tool.Refresh()

        #self.all.update()
        dirty = []
        dirty.append(self.sdecor.draw(self.screen))
        dirty.append(self.sunit.draw(self.screen))
        dirty.append(self.stext.draw(self.screen))
        for l in dirty:
            pygame.display.update(l)
        
        pygame.display.update()
        #pygame.display.flip()

        self.clock.tick(40)

    def Push_down(self,wo,xy):
        if wo == (1,0,0):
            #left click
            print xy
            print "sdecor",len(self.sdecor.draw(self.screen))
            print "images",len(self.images)

            #if "debug_image" not in dir(self):
            #    self.debug_image = [0]
            #k0 = self.images.keys()
            #k0.sort()
            #k = [x for x in k0 if "," in x and x.startswith("d_")]
            #print "k",k
            #j = self.debug_image[0]
            #self.debug_image[0]+=10
            #self.object_tool.Reset("u")
            #for i in range(10):
            #    ki = (j+i)%len(k)
            #    print "ki",ki,len(k)
            #    kk = k[ki]
            #    print kk
            #    self.object_tool.Set_object_quick("u1_test1_"+kk,kk,[100+(i*20),100],[0,0])
            #    self.object_tool.Set_object_quick("u1_test2_"+kk,kk,[100+(0*20),200],[0,0])
            #print "left click"
        else:
            print "click"

    def Write_text(self,t):
        #display the text t on the screen
        divided_by_line = t.split("$n")
        if "text" in self.text_obj.keys():
            self.text_obj["text"].Start_dying()
            #put it in dyingtext,
            #it's possible that there are parallel ones
            dtk = ""
            for i in range(100):
                if "dyingtext%03i"%i not in self.text_obj.keys():
                    dtk = "dyingtext%03i"%i
                    break
            self.text_obj[dtk] = self.text_obj["text"]
            del self.text_obj["text"]
        self.text_obj["text"] = Text(self,"text",[0,self.dim_menu+1],t)
        
        #there are only few possibilities:
        #1) the world wispers
        #2) spell description
        #3) geography description
        #4) left unit description
        #5) right unit description

    def Print_spell(self,spelled):
        spell = spelled[1]
        x,y = self.object_tool.pos3[spelled[0]]
        x = (x/self.text_length)-(len(spell)/2)
        y = (y/self.text_height)-6
        rv = random.random()
        self.text_obj["spell%i%f"%(spelled[0],rv)] = Text(self,"text",[x,y],spell)
        self.text_obj["spell%i%f"%(spelled[0],rv)].Start_dying(75)

    def Update_delays(self,dico):
        for k in dico.keys():
            self.chrono_obj.Add(dico[k],k)

    def Update_text(self,t):
        self.text_obj["text"].Update(t)

    def Finish(self):
        #if pygame.mixer:
        #    pygame.mixer.music.fadeout(100)
        pygame.time.wait(10)

class dummysound:
    def play(self): pass
    
    def load_sound(file):
        if not pygame.mixer: return dummysound()
        file = os.path.join('data', file)
        try:
            sound = pygame.mixer.Sound(file)
            return sound
        except pygame.error:
            print 'Warning, unable to load,', file
        return dummysound()

