#    This file is part of TWIM.
#
#    TWIM is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    TWIM is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with TWIM.  If not, see <http://www.gnu.org/licenses/>.

from twisted.internet import protocol, reactor
from twisted.internet.protocol import Factory, Protocol
from twisted.internet.endpoints import TCP4ClientEndpoint

class TS_Echo(protocol.Protocol):
    def __init__(self,g):
        """
        Part of the twisted server.
        Protocol
        """
        self.g = g
        self.inbox = []
        self.loop()

    def connectionMade(self):
        self.g.number_connection += 1
        if (self.g.verbose > 5): print ">Server, a new connection has been made (total:",self.g.number_connection,")"

    def connectionLost(self,e):
        self.g.number_connection += -1
        if (self.g.verbose > 5): print ">Server, a connection has been lost (total:",self.g.number_connection,")"
        if self.g.number_connection == 0 and self.g.standalone == 0:
            if (self.g.verbose > 5) :
                print "> there are no connection, and this is not a standalone server"
                print "> so, it should not run without any client"
                print "> -> close"
            reactor.stop()

    def dataReceived(self, data):
        for l in data.split("@@"):
            if l:
                self.inbox.append(l)
        
    def loop(self):
        if self.inbox:
            data = self.inbox[0]
            self.inbox = self.inbox[1:]
            returndata = self.g.treat_receiveddata(data)
            #returndata = "ok"
            if self.g.verbose > 5:
                if self.g.verbose > 8 or ( self.g.verbose > 5 and (len(data)>50 or len(returndata)>50) ):
                    print ">Server, received data:",data
                    print ">Server, returned data:",returndata
            self.transport.write("@@"+returndata)
        reactor.callLater(0.02, self.loop)
        
class TS_EchoFactory(protocol.Factory):
    def __init__(self, g):
        """
        Part of the twisted server.
        Factory
        """
        self.g = g
    
    def buildProtocol(self, addr):
        return TS_Echo(self.g)

class T_server():
    def __init__(self,port,password):
        """
        Twisted server class
        """        
        self.standalone = 0
        self.firstconnection_timeouttime = 3
        self.number_connection = 0
        self.treat_receiveddata = self.treat_receiveddata_dummy
        self.port = port
        self.verbose = 1
        self.password = password

    def treat_receiveddata_dummy(self,data):
        returndata = "ok"
        return returndata

    def timeout_firstconnection(self,t):
        if self.number_connection == 0:
            if (self.verbose > 5) :
                print ">Server, this is not a standalone server"
                print "> so, it should not run without any client"
                print "> 3 seconds have passed and no client is connected"
                print "> -> close"
            reactor.stop()
    
    def start(self):
        if (self.verbose > 5): print ">Server, start"

        reactor.listenTCP(self.port, TS_EchoFactory(g))
        if self.standalone == 0: reactor.callLater(self.firstconnection_timeouttime, self.timeout_firstconnection, "")
        reactor.run()


class TC_Greeter(Protocol):
    def __init__(self, g):
        """
        Part of the twisted client.
        Protocol
        """
        self.g = g
        
    def sendMessage(self, msg):
        if self.g.verbose > 8 or ( self.g.verbose > 5 and len(msg)>50 ):
            print ">Client, sending data:",msg
        self.transport.write("@@"+msg)

    def dataReceived(self, data):
        for l in data.split("@@"):
            if not l:
                continue
            if self.g.verbose > 8 or ( self.g.verbose > 5 and len(l)>50 ):
                print ">Client, receiving data:",l
            self.g.inbox.append(l)

    def connectionMade(self):
        #print "a new connection is made"
        self.g.connected = 2

    def connectionLost(self,e):
        #print "the connection is lost",e
        self.g.connected = 0

class TC_GreeterFactory(Factory):
    def __init__(self,g):
        """
        Part of the twisted client.
        Factory
        """
        self.g = g
    
    def buildProtocol(self, addr):
        return TC_Greeter(g)

class T_client():
    def __init__(self,port,password):
        """
        Twisted client class
        """        
        self.point = -1
        self.d = -1
        self.p = -1
        self.outbox = []
        self.inbox = []
        self.connected = 0
        self.nextFct = self.dummynFct
        self.url = "localhost"
        self.port = port
        self.verbose = 1
        self.password = password
        self.connect_attempt = 0

    def dummynFct(self):
        if (self.verbose > 5): print ">Client, has been started"

    def gotProtocol(self,p):
        #reactor.callLater(1, p.sendMessage, "This is sent in a second")
        if (self.verbose > 5):
            print ">Client, gotProtocol"
        self.p = p
        self.connected = 1

    def Queue(self):
        #send what inside the outbox
        if self.connected:
            if self.outbox:
                message = self.outbox[0]
                self.outbox = self.outbox[1:]
                self.p.sendMessage(message)
            reactor.callLater(0.02, self.Queue)
        else:
            self.connect_attempt += 1
            if (self.verbose > 5): 
                print ">Client, not connected",self.connect_attempt
            if self.connect_attempt >= 5:
                if (self.verbose > 5): 
                    print "> -> leave"
                reactor.stop()
            else:
                reactor.callLater(0.02, self.Queue)

    def doNext(self):
        reactor.callInThread(self.nextFct)        

    def start(self):
        self.point = TCP4ClientEndpoint(reactor, self.url, self.port)
        #reactor.callLater(1, CheckConnection)
        self.d = self.point.connect(TC_GreeterFactory(self))
        self.d.addCallback(self.gotProtocol)
        reactor.callLater(0.02, self.Queue)
        reactor.callLater(0.04, self.doNext)
        reactor.run()

def main_server_loop():
    global o_serv
    if o_serv:
        o_serv.Loop()
    reactor.callLater(0.1, main_server_loop)

def main_server():
    global g,o_serv,port,password,verbose
    g = T_server(port,password)
    g.verbose = verbose
    import twim_server
    o_serv = twim_server.Server(g)
    main_server_loop()
    if ("CServer" in sys.argv):
        g.standalone = 1
    else:
        g.standalone = 0
        g.firstconnection_timeouttime = 3
    if "--load" in sys.argv:
        o_serv.file_to_load = sys.argv[sys.argv.index("--load")+1]
    g.start()

def main_igclient():
    global g,port,password,verbose
    g = T_client(port,password)
    g.verbose = verbose
    import twim_client
    o = twim_client.Client(g)
    o.client_name = "robert"
    o.reactor = reactor
    g.start()
        
def main_iaclient():
    global g,port,password,verbose
    g = T_client(port,password)
    g.verbose = verbose
    import twim_ainorbert
    o = twim_ainorbert.Client(g)
    o.reactor = reactor
    g.start()


if __name__ == "__main__":
    import sys
    import os
    
    g=-1
    port = 1205
    password = "dummymdp"
    
    verbose = 0
    if "Verb" in os.environ.get("TWIM_DEBUG",""):
        verbose = int(os.environ.get("TWIM_DEBUG","").split("Verb")[1][0])
    
    #password_letters = "abcdefghijklmnopqrstuvwxyz"
    #password_letters += password_letters.upper()
    #password_letters += "01234567890!.-,=;:+%"
    #self.mdp = ""
    #for i in range(random.randint(5,10)):
    #    self.mdp += password_letters[random.randint(0,len(password_letters)-1)]
    
    
    if "-h" in sys.argv:
        print "Module to start networking component"
        print " CServer  : server in standalone version"
        print " IGServer : server 'in game' mode"
        print " IGClient : client 'in game' mode"
        print " AIClient : client for AI"
    elif ("CServer" in sys.argv) or ("IGServer" in sys.argv):            
        main_server()
    elif "IGClient" in sys.argv:
        main_igclient()
        #import cProfile as profile
        #profile.run('main_igclient()')
    elif "AIClient" in sys.argv:
        main_iaclient()

