#    This file is part of TWIM.
#
#    TWIM is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    TWIM is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with TWIM.  If not, see <http://www.gnu.org/licenses/>.

"""
This class deal with the choices done by the animals
This is used by
twim_server
"""

import random
import math
import os

class Behavior:
    def __init__(self,w,u):
        self.world = w
        self.unit_name = u
        self.what = None
        self.level = None
        unit = self.Get_unit()
        self.current_action = "sleep"
        self.ca = {}
        self.around = [None,None]

    def __getstate__(self):
        """
        Veto attributes that should not be saved in files
        """
        pickle_veto = ["world"]
        return {k: v for k, v in self.__dict__.iteritems() if k not in pickle_veto}

    def Get_unit(self):
        """
        safe way to get the unit in a consistent way
        """
        unit = None
        try:
            unit = self.world.units[self.unit_name]
            if self.what == None:
                self.what = self.world.structure_unit[unit.what][0]
            if self.level == None:
                self.level = unit.level
        except:
            return None
        return unit

    def Loop(self):
        cu = self.Get_unit()
        if not cu:
            return
        kno = cu.knowledge[:]
        cu.knowledge = []

        #1) right now, only ANI_LOCAL supported
        if self.what != "ANI_LOCAL":
            return

        #2) look for changes
        changes = []
        # in knowledge
        if kno:
            for l in kno:
                changes += self.Add_knowledge(l)
        # in neighbourg
        rkn = cu.room_keyname
        di,di_l,di_r = self.world.rooms[rkn].Get_info(cu.name)
        if di_l.get("name") and di_l.get("name") != self.around[0]:
            self.around[0] = di_l.get("name")
            changes += ["lu"]
        if di_r.get("name") and di_r.get("name") != self.around[1]:
            self.around[1] = di_r.get("name")
            changes += ["ru"]

        #3) decide the actions
        if changes or self.current_action == "todecide":
            self.Decide(changes)

    def Add_knowledge(self,k):
        #print "new knowledge",k
        if k.startswith("attacked"):
            return [k]
        return []

    def Decide(self,changes):
        #1) what is the level of thread
        threat = 0
        delay = 5
        for eui in self.around:
            eu = self.world.units.get(eui)
            u1w = ""
            if eu:
                u1w = self.world.structure_unit[eu.what][0]
            if self.what == "ANI_LOCAL" and u1w == "PLAYER":
                threat += 5

        for c in changes:
            if c.startswith("attacked"):
                threat += 10
                delay += -1
            if c in ["lu","ru"]:
                delay += 1

        if not self.ca.get("threat"):
            self.ca["threat"] = threat-100

        choice = []
        if threat != self.ca["threat"]:
            #thread on the subject increased
            #re-evaluate
            #3 possibilities:
            # 1) flee (only for ani_move)
            # 2) defend
            # 3) attack

            dico1 = []
            #attack
            for eui in self.around:
                if not eui:
                    continue
                dico2 = self.Get_weighted_strategy(5,eui)
                dico1 += [[x[0]+threat]+x[1:] for x in dico2]
            #defend
            dico1 += self.Get_weighted_strategy(3)
            dico1.sort()
            if self.world.nature.verbose == 3:
                print "Decide, list",dico1
            if dico1:
                if dico1[-1][0] > 5+(20/(self.level+1)):
                    choice = dico1[-1]

        #compare the possible new choice with the current one
        #if the strat is different, change strategy
        if choice:
            if choice[1] != self.current_action:
                self.current_action = choice[1]
                self.ca["time"] = int((delay*math.sqrt(10-self.level))+random.randint(0,5))
                self.ca["spell"] = choice[2]
                if choice[1] == "attack":
                    self.ca["name"] = choice[3]
        else:
            self.current_action = "sleep"
            


    def Get_weighted_strategy(self,what,other=None):
        cu = self.Get_unit()
        spells = cu.Get_affordable_spells(what)
        a = []
        if what == 5:
            attacker = cu
            attackee = None
            try:
                attackee = self.world.units[other]
            except:
                return []

            random.shuffle(spells)
            candidate = []
            for k in spells:
                sp = self.world.spells[k]
                vr = [max(-1,attacker.statusS[i]-sp.val1[i]) for i in range(6) if sp.val1[i] != 0]
                ve = [max(0,attackee.statusH[i]-sp.val2[i]) for i in range(6) if attackee.statusH[i] != 0 and sp.val2[i] != 0]
                score = 0
                if vr.count(-1):
                    score = 0
                else:
                    if ve.count(0):
                        score += 200
                    ve.sort()
                    if ve:
                        score += 50./(ve[0]+1)
                    for j in range(20):
                        vr = [max(-1,attacker.statusS[i]-(j*sp.val1[i])) for i in range(6) if sp.val1[i] != 0]
                        ve = [max(0,attackee.statusH[i]-(j*sp.val2[i])) for i in range(6) if attackee.statusH[i] != 0 and sp.val2[i] != 0]
                        if ve.count(0):
                            score += 100./(1+j)
                            break
                        if vr.count(-1):
                            score += -10./(1+j)
                            break
                score += random.random()
                score *= 1+(0.1*self.level)
                a.append([score,"attack",k,attackee.name])

        if what == 3:
            needed = [-1,-1,-1,-1,-1,-1]
            for i in range(6):
                if cu.statusH[i] == 0:
                    continue
                needed[i] = 1./( 1+math.exp( 0.3*(cu.statusH[i]-15) ) )
                #closer to 1 means more needed
                #before 15: we need it, after 15: it's ok
            random.shuffle(spells)
            for k in spells:
                sp = self.world.spells[k]
                vr = [max(-1,cu.statusS[i]-sp.val1[i]) for i in range(6) if sp.val1[i] != 0]
                if vr.count(-1):
                    continue
                if [1 for i in range(6) if cu.statusH[i]==0 and sp.val2[i]!=0]:
                    continue
                vg = [needed[i]*needed[i]*20*sp.val2[i] for i in range(6)]
                score = sum(vg)-sum(sp.val1)
                #score += sum([i*i*6 for i in needed])
                a.append([score,"defend",k])
        return a

    def Get_decision(self):
        a = ["sleep"]
        if self.current_action == "sleep":
            return ["sleep"]
        else:
            if not self.ca.get("time"):
                self.ca["time"] = -1
            self.ca["time"] += -1
            if self.ca["time"] < 0:
                if self.current_action == "attack":
                    a = ["attack",self.ca.get("name"),self.ca.get("spell")]
                elif self.current_action == "defend":
                    a = ["defend",self.ca.get("spell")]
                self.current_action = "todecide"
        return a


class Nature:
    def __init__(self,w):
        """
        class to deal with what happens with the other non-player units:
        1) the ani_local, ani_move will take decision
        2) the units will regenerate
        3) new units will appears
        """
        self.world = w
        self.run_iterator = -1
        self.behaviors = {}
        self.verbose = 0
        if os.environ.get("TWIM_DEBUG",""):
            if "Verb" in os.environ.get("TWIM_DEBUG",""):
                self.verbose = int(os.environ.get("TWIM_DEBUG","").split("Verb")[1][0])

    def __getstate__(self):
        """
        Veto attributes that should not be saved in files
        """
        pickle_veto = ["world"]
        return {k: v for k, v in self.__dict__.iteritems() if k not in pickle_veto}

    def Loop(self):
        """
        This function is called every 0.1 second in a parallel thread
        Don't block it
        Right now: the more units there is, the slower this is. TODO: change that
        """
        for i in range(10):
            self.run_iterator+=1
            if self.run_iterator >= len(self.world.units):
                self.run_iterator = 0
            if len(self.world.units) == 0:
                break
            un = self.world.units.keys()[self.run_iterator]
            b = self.behaviors.get(un)
            if not b:
                self.behaviors[un] = Behavior(self.world,un)
                b = self.behaviors.get(un)
            b.Loop()
            self.Proceed_decision(b.Get_decision(),b.Get_unit())

    def Proceed_decision(self,decision,cu):
        if decision[0] == "sleep":
            pass
        elif decision[0] == "attack":
            name_to_attack = decision[1]
            spell = decision[2]
            r1,r2 = self.world.server.Play_spell(spell,[name_to_attack],cu.name)
        elif decision[0] == "defend":
            spell = decision[1]
            r1,r2 = self.world.server.Play_spell(spell,[],cu.name)



