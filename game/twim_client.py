#    This file is part of TWIM.
#
#    TWIM is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    TWIM is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with TWIM.  If not, see <http://www.gnu.org/licenses/>.

import time
import os
from random import *
from math import *
import ast

#import basic pygame modules
import pygame
from pygame.locals import *

from twim_display import *
from twim_util import *

class Client():
    def __init__(self,g):
        """
        Main class for the client part of the game
        Does not contains the pygame specific stuffs, which are in the Display class
        But contains all the communication with the server

        arguments:
            g: P_Client class, in order to access the network infrastructure
        """
        
        self.g = g                      # P_Client class
        self.reactor = -1               # the reactor used by the twisted library
        self.g.nextFct = self.Loop      # function called after the client makes the connection

        self.client_name = "robert"     # user name, used for the connection with the server
        self.client_name += str(randint(0,100))
        password_letters = "abcdefghijklmnopqrstuvwxyz"
        password_letters += password_letters.upper()
        password_letters += "01234567890!.-,=;:+%"
        self.client_password = ""       # client password, used for the connection with the server
        for i in range(randint(5,10)):
            self.client_password += password_letters[randint(0,len(password_letters)-1)]

        self.status = 0                 # status of the connection (0: init, 1: get the first complete update, 2: ask if small update, ...)
        
        self.modules_unit,self.modules_decor = Get_ressources() #load all the ressources for decor (room) and unit

        self.display = Display(self)    # Display class

        self.time = 0                   # time in game, sent by the server
        self.sessionname = ""           # name of the session, send by the server

        self.current_room = {}          # just a dico with the room important info
                                        # 'name', 'style'
        self.current_player = {}        # just a dico with the player important info
                                        # 's values', 'h values'
        self.current_unit_left = {}     # just a dico with the left unit import info
                                        # 'name', 'what', 'status'
        self.current_unit_right = {}    # just a dico with the right unit import info
                                        # 'name', 'what', 'status'
        self.current_view = {}          # just a dico with the current unit that we are looking at
                                        # 'name', 'what', 'statusS', 'statusH', ...

        self.print_current = ""         # name of the thing that is currently watched, for auto-update

        self.old_status = [None,None,""]       # needed for the summary when we change room

        self.history_cursor = -1        # position of the history, -1 if not looking at history right now
        self.history_content = {}       # content of the history for each things
        self.history_list = []          # list of things in the history

        self.ticks = 0                  # ticks from the main loop

        self.playing = 1

    def Loop(self):
        """
        Loops on the main update functions (send and receive update request and update the view)
        status 0 = init connection
        status 1 = create the game environment
        status 2 = we are playing, we get response of update request (UPok) and command feedback (COok)
        """

        self.playing = 1

        #init with the server
        self.Send_server("WTP")

        while self.playing:

            self.ticks += 1
            if self.ticks > 1000:
                self.ticks = 0

            #check if the reactor is still running
            #if not, quit
            if self.reactor.running == 0:
                self.playing = 0
                break

            if self.status == 0:
                #check if the server accepts the connection
                for l in self.g.inbox:
                    t = l.split("@")[-1]
                    if t == "NPok":
                        self.status = 1
                        self.Send_server("UPInit")
                        self.g.inbox.remove(l)
                        break
            elif self.status == 1:
                #check if the server accepts the connection
                li = self.g.inbox
                for l in li:
                    t = l.split("@")[-1]
                    if t[:5] == "UPIok":
                        self.status = 2
                        self.Init_environ(t[5:])
                        self.g.inbox.remove(l)
                        break
            elif self.status == 2:
                self.Send_server("UP")
                li = self.g.inbox
                for l in li:
                    t = l.split("@")[-1]
                    if t.startswith("UPok"):
                        self.Update(t[4:])
                        self.g.inbox.remove(l)
                    elif t.startswith("COok"):
                        self.Update(t[4:])
                        self.g.inbox.remove(l)
                    elif t.startswith("ROok"):
                        self.Update(t[4:])
                        self.g.inbox.remove(l)
                    elif t.startswith("DUok"):
                        self.Update(t[4:])
                        self.g.inbox.remove(l)

            if self.display.status:
                self.display.Update_view()
                
            if self.print_current.startswith("FU"):
                #force update
                if self.ticks%20 == 0:
                    if self.print_current.startswith("FUs"):
                        #update a spell
                        hw = "spell_"+self.print_current[3:]
                        self.Print(self.history_content[hw],0,1)

        self.display.Finish()

        if self.reactor.running: self.reactor.stop()

    def End_game(self):
        """
        The player is dead
        """
        self.Print({"type":"text","text":"well, you are dead$nthe end"})

    def Enter(self,t):
        """
        A direct command from the display

        arguments:
           t: the text typed on the display
        """
        self.Send_server("CO"+t)

    def Ask_unit_description(self,ori):
        """
        Ask the server to return a description of the unit
        
        arguments:
          ori: 0=left, 1=right
        """
        if ori == 0:
            name = self.current_unit_left.get("name","")
        elif ori == 1:
            name = self.current_unit_right.get("name","")
        if name:
            self.Send_server("DU"+name)

    def Send_server(self,t):
        """
        Fills the outbox with a message to be sent to the server
        The message will be formated into paquet here

        arguments:
           t: the message to be sent
        """
        t1 = self.g.password
        t2 = self.client_name
        t3 = self.client_password
        self.g.outbox.append(t1+"@"+t2+"@"+t3+"@"+t)

    def Init_environ(self,t):
        """
        Initializes the environnement after the first contact with the server

        arguments:
           t: message received
        """
        stuffs = t.split("#")
        version = stuffs[0]
        self.display.cl = ast.literal_eval(stuffs[1])
        self.display.Init()
        self.display.Change_decor()
        t = "#".join(stuffs[3:])
        self.Update(t)
        if version != VERSION:
            print "The server version is not the same as the client"
            print "Client version is",VERSION
            print "Server version is",version
            print "This can led to problems"
            self.Print({"type":"text","text":"warning, incompatible versions$nclient:"+VERSION.replace("."," ")+"$nserver:"+version.replace("."," ")+"$nthis can lead to problems"})
        self.Print(stuffs[2][2:])

    def Print_view(self):
        """
        Pass self.current_view to Print so it can be printed
        """

        info = self.current_view.copy()
        info["type"] = "unit"

        self.Print(info)

    def Print(self,info,fillhistory=1,update=0):
        """
        Transform info from the server on text to be displayed

        argument:
        info: text which is repr(dico)
              or directly the corresponding dico
        fillhistory: =1 if the new printed this is put into the history
        """

        self.print_current = "  "

        #print "info",info
        if type(info) == type(""):
            d = ast.literal_eval(info)
        else:
            d = info
        text = "uh?"
        history_thing = ""
        if d.get("type") == "first":
            history_thing = "first"
            text = "the world whispers:$n$b%s$e$nand$n$b%s$e"%(d.get("1",""),d.get("2",""))
        elif d.get("type") == "environ":
            history_thing = "environ_%s"%d.get("here","")
            text = "you stand in $b%s$e$n$n"%d.get("here","")
            it = ["north","east","south","west"]
            for i in it:
                if not d.get(i+"_name"):
                    continue
                cl = ""
                for ii in range(6):
                    rcl = d.get(i+"_cl",[0,0,0,0,0,0])
                    if rcl[ii] != 0:
                        cl += "$"+str(ii+1)+str(rcl[ii])+"$e"
                text += "%s:$n $b%s$e$n %s$n$n"%(i,d.get(i+"_name",""),cl)
        elif d.get("type") == "spell":
            history_thing = "spell_%s"%d.get("name","")
            what = d.get("what",0)
            text = "$b%s$e$n"%d.get("name","")
            rcl1 = d.get("val1",[0,0,0,0,0,0])
            rcl2 = d.get("val2",[0,0,0,0,0,0])
            cl1,cl2 = "",""
            if "modifier" in d.keys():
                text += "modified "
            for ii in range(6):
                if rcl1[ii] != 0:
                    cl1 += "$"+str(ii+1)+str(rcl1[ii])+"$e"
                if rcl2[ii] != 0:
                    cl2 += "$"+str(ii+1)+str(rcl2[ii])+"$e"
            if what == 1:
                text += "knowledge word$n"
                text += "$n"
                text += "instant knowledge of the word$nthat follows"
            elif what == 2:
                text += "space word$n"
                text += "$n"
                text += "where you are,$n"
                text += "instant travel to the place that follows"
            elif what == 3:
                text += "protection word$n$n"
                text += "will add %s to your $bhand$e$n"%cl2
                text += "for a cost of %s$n"%cl1
            elif what == 4:
                clH = self.current_player.get("statusH",[0,0,0,0,0,0])
                clS = self.current_player.get("statusS",[0,0,0,0,0,0])
                rr = [0,0,0,0,0,0]
                gr = [0,0,0,0,0,0]
                maxi = 0
                for i in range(6):
                    if clH[i]-rcl1[i] >= 0:
                        rr[i] = rcl1[i]
                    else:
                        maxi = 1
                        rr[i] = clH[i]
                    gr[i] = (rr[i])-rcl2[i]
                    if gr[i] < 0: gr[i] = 0
                cl1b,cl2 = "",""
                for ii in range(6):
                    if rr[ii] != 0:
                        cl1b += "$"+str(ii+1)+str(rr[ii])+"$e"
                    if gr[ii] != 0:
                        cl2 += "$"+str(ii+1)+str(gr[ii])+"$e"
                if cl1b == "":
                    cl1b = "0"
                if cl2 == "":
                    cl2 = "0"
                text += "retreat word$n$n"
                text += "will remove %s to your $bhand$e"%cl1b
                if maxi:
                    text += ", instead of %s"%cl1
                text += "$nfor a gain of %s$n"%cl2
                #text += "$nif a color is not enough,$n"
                #text += " the removal is still done$n"
                #text += " but the gain for this color is null$n"
            elif what == 5:
                text += "attack word$n$n"
                text += "will attack the creature that follows$n"
                text += "with a force of %s$n"%cl2
                text += "for a cost of %s$n"%cl1
            elif what == 6:
                text += "creation word$n$n"
                text += "will create %s$n"%cl2
                text += "for a cost of %s$n"%cl1
            elif what == 7:
                text += "conversion word$n$n"
                rcl1 = d.get("val1",[0,1,2,3,4,5])
                for i in range(6):
                    if rcl1[i] != i:
                        text += "every $"+str(i+1)+" $e is treated like $"+str(rcl1[i]+1)+" $e$n"
                text += "for a cost of %s$n"%cl2
            if d.get("delay",0):
                text += "$ndelay of %s"%self.display.Transform_time(d.get("delay",0),1)
            if d.get("name","") in self.current_player.get("spell_delays",{}).keys():
                delay = self.current_player.get("spell_delays",{})[d.get("name","")]
                if delay-self.time > 0:
                    text += "$nwill be active in %s"%self.display.Transform_time(delay-self.time,1)
                    self.print_current = "FU"
        elif d.get("type") == "text":
            text = d.get("text","")
            if text:
                history_thing = "text_%i"%( hash(text) )
            else:
                history_thing = "text_"       
        elif d.get("type") == "unit":
            if d.get("isdead"):
                text = ""
                history_thing = ""
                fillhistory = 0
            else:
                history_thing = "unit_%s"%d.get("name","")
                what = d.get("what","")
                u = self.modules_unit.get(what)
                text = what
                if u:
                    text = u(None).Describe(d)

        if history_thing.startswith("unit_"):
            self.print_current += "u"+history_thing[5:]
        if history_thing.startswith("spell_"):
            self.print_current += "s"+history_thing[6:]

        #add to the history
        if fillhistory:
            self.Add_history(history_thing,d)

        if update:
            self.display.Update_text(text)
        else:
            self.display.Write_text(text)
    
    def Update_special(self,t):
        """
        Subfunction of update.
        Deal with special messages from the server (starting with !)
        which require more work than just change a value of an object

        arguments:
           t: message received
        """
        pass

    def Update_stuff(self,obj,dico,fct):
        """
        Update some dictionaries with the value of another dictionnary
        and call a function is something has changed
        
        Argument:
        obj: the dico object of self
        dico: the dico that contains modification
        fct: the function to call when the modification has been done
        """
        updated = 0
        keys = dico.keys()
        if obj == {}:
            for k in keys:
                obj[k] = dico[k]
            updated = 1
        else:
            for k in keys:
                if k in obj.keys():
                    if obj[k] != dico[k]:
                        obj[k] = dico[k]
                        updated = 1
                else:
                    obj[k] = dico[k]
                    updated = 1
        if updated:
            fct()

    def Move_history(self,dire):
        go = 0
        if dire == "up":
            if len(self.history_list) > 1:
                if self.history_cursor == -1:
                    self.history_cursor = len(self.history_list)-1
                self.history_cursor += -1
                if self.history_cursor < 0:
                    self.history_cursor = 0
                else:
                    go = 1
        elif dire == "down":
            if self.history_cursor != -1:
                self.history_cursor += 1
                if self.history_cursor >= len(self.history_list):
                    self.history_cursor = -1
                else:
                    go = 1
        if go:
            hw = self.history_list[self.history_cursor]
            self.Print(self.history_content[hw],0)

    def Add_history(self,what,content):
        self.history_cursor = -1
        self.history_content[what] = content
        if what in self.history_list:
            self.history_list.remove(what)
        self.history_list.append(what)

    def Print_summaryaftermove(self):
        text = ""
        room1 = self.current_room["name"]
        if self.old_status[0] and self.old_status[2]:
            status1 = self.current_player["statusS"]
            status2 = self.current_player["statusH"]
            status3 = self.old_status[0]
            status4 = self.old_status[1]
            room2 = self.old_status[2]
            st1,st2,st3,st4 = "","","",""
            for ii in range(6):
                if status1[ii] != 0:
                    st1 += "$"+str(ii+1)+str(status1[ii])+"$e"
                if status2[ii] != 0:
                    st2 += "$"+str(ii+1)+str(status2[ii])+"$e"
                if status3[ii] != 0:
                    st3 += "$"+str(ii+1)+str(status3[ii])+"$e"
                if status4[ii] != 0:
                    st4 += "$"+str(ii+1)+str(status4[ii])+"$e"

            self.old_status = [status1[:],status2[:],room1]
            text = "you are now in $b%s$e$n$n"%(room1)
            text += "in $b%s$e you went from$n%s$n%s$n"%(room2,st3,st4)
            text += "to$n%s$n%s$n"%(st1,st2)
        self.Print({"type":"text","text":text})
        self.old_status[2] = room1

    def Update(self,t):
        """
        At each update, this fonction is called and it checks the incoming message
        to update the object accordingly

        arguments:
           t: message received
        """
        if t.startswith("!T"):
            self.Print(t[2:])

        #we could use eval, but this is not safe
        t1 = t.split("#")
        move_room = 0
        for l in t1:
            if not l: continue
            if l[0] == "!":
                #special command
                self.Update_special(l)
            if len(l.split("="))!=2: continue
            obj = l.split("=")[0]
            pro = l.split("=")[1]
            cu = -1
            #if obj != "time":
            #    print "obj",obj
            if obj == "time":
                self.time = ast.literal_eval(pro)
            elif obj == "sessionname":
                self.sessionname = ast.literal_eval(pro)
            elif obj == "room":
                name1 = self.current_room.get("name","")
                self.Update_stuff(self.current_room,ast.literal_eval(pro),self.display.Change_decor)
                name2 = self.current_room.get("name","")
                if name1 != name2:
                    move_room = 1
            elif obj == "player":
                self.Update_stuff(self.current_player,ast.literal_eval(pro),self.display.Refresh_status)        
                if len(self.print_current)>2:
                    if self.print_current[2] == "s" and self.print_current[3:] in ast.literal_eval(pro).get("spell_delays",{}).keys():
                        hw = "spell_"+self.print_current[3:]
                        self.Print(self.history_content[hw],0,1)
                if not self.old_status[0]:
                    self.old_status[0] = self.current_player["statusS"]
                    self.old_status[1] = self.current_player["statusH"]
                if ast.literal_eval(pro).get("spell_delays",None):
                    self.display.Update_delays(ast.literal_eval(pro).get("spell_delays",None))
            elif obj == "unitl":
                self.Update_stuff(self.current_unit_left,ast.literal_eval(pro),self.display.Set_unit_left)        
            elif obj == "unitr":
                self.Update_stuff(self.current_unit_right,ast.literal_eval(pro),self.display.Set_unit_right)        
            elif obj == "view":
                self.Update_stuff(self.current_view,ast.literal_eval(pro),self.Print_view)
            elif obj == "viewr":
                if self.print_current[3:] == ast.literal_eval(pro)["name"]:
                    self.Update_stuff(self.current_view,ast.literal_eval(pro),self.Print_view)
            elif obj == "viewl":
                if self.print_current[3:] == ast.literal_eval(pro)["name"]:
                    self.Update_stuff(self.current_view,ast.literal_eval(pro),self.Print_view)
            elif obj == "spelled":
                self.display.Print_spell(ast.literal_eval(pro))
            elif obj == "killed":
                print "client knows kill"
                self.display.Show_killed(ast.literal_eval(pro))
                if self.print_current[3:] == ast.literal_eval(pro)[1]:
                    self.current_view["isdead"] = 1
                    self.Print_view()
        if move_room:
            self.Print_summaryaftermove()


