import random
from math import *
import time

class Unit:

    NAME="grimoire"
    RARETY=0#the highest, the rarer
    TYPES=["CONT_SP_O"]#cf. twim_world
    TYPE=""
    IMAGES=["grimoire0.png","grimoire1.png"]

    def __init__(self,parent):
        self.parent = parent
        #parent can be twim_object or twim_world
        #depending on the function called

    def Load(self,i,o,pos):
        #i: 0 = left, 1 = center, 2 = right
        #   i is also used in pos3[i] and unit_obj[i]
        #o: 0 is to the left, 1 is to the right
        #pos: position
        parent = self.parent
        mod = {"S":1,"A":0,"I":0,"T":0}
        parent.Set_object("u"+repr(i)+"_grimoire","u_grimoire_grimoire0",[pos[0],pos[1]],[20,50],mod)

        self.pos = pos[:]
        self.i = i
        mod = {"S":1,"A":0,"I":0,"T":100}
        parent.Set_object("u"+repr(i)+"_grimoirestar0","u_grimoire_grimoire1",[-100,pos[1]-50],[0,0],mod)
        parent.Set_object("u"+repr(i)+"_grimoirestar1","u_grimoire_grimoire1",[-100,pos[1]-50],[0,0],mod)
        parent.Set_object("u"+repr(i)+"_grimoirestar2","u_grimoire_grimoire1",[-100,pos[1]-50],[0,0],mod)
        parent.Set_object("u"+repr(i)+"_grimoirestar3","u_grimoire_grimoire1",[-100,pos[1]-50],[0,0],mod)
        parent.Set_object("u"+repr(i)+"_grimoirestar4","u_grimoire_grimoire1",[-100,pos[1]-50],[0,0],mod)

        mo = parent.Get_obj_objfromname("u"+repr(i)+"_grimoire")
        mo.Add_child("u"+repr(i)+"_grimoirestar0",[0,0])
        mo.Add_child("u"+repr(i)+"_grimoirestar1",[0,0])
        mo.Add_child("u"+repr(i)+"_grimoirestar2",[0,0])
        mo.Add_child("u"+repr(i)+"_grimoirestar3",[0,0])
        mo.Add_child("u"+repr(i)+"_grimoirestar4",[0,0])

        a4 = [{"type":"mod","mod":{"T":10}},{"type":"loop"}]
        nm4 = "grimoire_star_fade"+repr(i)
        parent.Set_motion(nm4,a4)
        for ii in range(5):
            object_name = "u"+repr(i)+"_grimoirestar%i"%ii
            parent.motions[nm4].list_object.append(object_name)
            a5 = [{"type":"random_wait","time1":30,"time2":40},{"type":"fct","fct":self.reinitstar},{"type":"loop"}]
            nm5 = "grimoire_star_reinit"+repr(i)+"_"+repr(ii)
            parent.Set_motion(nm5,a5)
            parent.motions[nm5].list_object.append(object_name)

    def reinitstar(self,object_name):
        x,y = self.pos[0],self.pos[1]-350-random.randint(0,10)
        x = x+random.randint(0,30)-17
        ina = self.parent.unit_obj[self.i][object_name].Modify_name("u_grimoire_grimoire1",{"T":0},1)
        self.parent.unit_obj[self.i][object_name].image_name = ina
        self.parent.unit_obj[self.i][object_name].Apply_motion_absolutemove([x,y],{"T":0})

    def Describe(self,info):
        sp = info.get("spells",[])
        if len(sp)==1:
            text = "this grimoire contains the following word$n"
        elif len(sp)>1:
            text = "this grimoire contains the following words$n"
        else:
            text = "this grimoire is empty"
        text += "$n"
        for l in sp:
            text += "$b%s$e$n"%l
        return text

    def Fill(self, unit, room):
        w = room.world
        for i in range(10):
            sp = w.Find_spell([3,4,5],[0,1,2,3],room.style[0])
            if sp:
                if sp not in unit.spells:
                    unit.spells.append(sp.name)
                    print sp.name,sp.what
