import random
from math import *
import time

class Unit:

    NAME="wizard"
    RARETY=0#the highest, the rarer
    TYPES=["PLAYER"]#cf. twim_world
    TYPE=""
    IMAGES=["wizard_body.png"]
    IMAGES+=["wizard_head.png"]
    IMAGES+=["wizard_arm.png"]
    IMAGES+=["wizard_stick.png"]
    #X.png of the unit named Y will be named u_Y_X

    def __init__(self,parent):
        self.parent = parent

    def Load(self,i,o,pos):
        #i: 0 = left, 1 = center, 2 = right
        #   i is also used in pos3[i] and unit_obj[i]
        #o: 0 is to the left, 1 is to the right
        #pos: position
        parent = self.parent
        mod = {}
        if o == 0:
            mod = {"I":1}
        parent.Set_object("u"+repr(i)+"_wizard0","u_wizard_wizard_body",[pos[0],pos[1]+10],[18,65],mod)
        parent.Set_object("u"+repr(i)+"_wizard1","u_wizard_wizard_head",[pos[0],pos[1]+10],[18,65],mod)
        parent.Set_object("u"+repr(i)+"_wizard2","u_wizard_wizard_arm",[pos[0],pos[1]+10],[18,65],mod)
        parent.Set_object("u"+repr(i)+"_wizard3","u_wizard_wizard_stick",[pos[0],pos[1]+10],[18,65],mod)
        mo = parent.Get_obj_objfromname("u"+repr(i)+"_wizard0")
        mo.Add_child("u"+repr(i)+"_wizard1",[0,0])
        mo.Add_child("u"+repr(i)+"_wizard2",[0,0])
        mo.Add_child("u"+repr(i)+"_wizard3",[0,0])

        #a1 = [{"type":"random_wait","time1":150,"time2":500}]
        #pattern = [-1,-1,0,-1,0,0,0,0,0,1,0,1,1]
        #for pi in range(len(pattern)):
        #    if pattern[pi] == 0:
        #        a1.append({"type":"empty"})
        #    else:
        #        mod = {"A":pattern[pi]}
        #        a1.append({"type":"mod","mod":mod})
        #a1.append({"type":"loop"})
        #parent.Set_motion("wizardhead%i_main"%i,a1)
        #parent.motions["wizardhead%i_main"%i].list_object = ["u"+repr(i)+"_wizard1"+repr(o)]

        #a2 = [{"type":"random_wait","time1":1,"time2":2},{"type":"mod","mod":{"A":1}},{"type":"mod","mod":{"A":-1}},{"type":"loop"}]
        #parent.Set_motion("wizardbody%i_main"%i,a2)
        #parent.motions["wizardbody%i_main"%i].list_object = ["u"+repr(i)+"_wizard0"+repr(o)]

    def Describe(self,info):
        pass

    def Fill(self,room):
        pass



