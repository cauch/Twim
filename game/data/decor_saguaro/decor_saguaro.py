import random
from math import *
import time

class Decor:

    NAME="saguaro"
    IMAGES=["cache.png","cactustrunk1.png","cactusbranch1.png","sun.png","ground1.png","ground2.png","m1.png","m2.png","m3.png","m4.png"]
    RARETY=2#the highest, the rarer
    #X.png of decor named Y will be named d_Y_X

    def __init__(self,parent):
        self.parent = parent


    def Load(self,style_cl,style_rs):
        parent = self.parent
        seed_continuity = random.random()
        random.seed(style_rs)
        d = self.parent.display
        mod = {"S":1,"A":0,"I":0,"T":0}
        parent.Set_object("d_g04bg1","bkgd_%i"%style_cl,[0,0],[0,0],mod)

        parent.Set_object("d_g04sun","d_saguaro_sun",[random.randint(250,765),random.randint(40,140)],[100,100],{})

        #1) add ground
        mod = {"S":1,"A":0,"I":0,"T":0}
        parent.Set_object("d_g04ground1","d_saguaro_ground1",[random.randint(-30,240),400],[0,0],mod)
        mod = {"S":1,"A":0,"I":0,"T":70}
        parent.Set_object("d_g04ground2","d_saguaro_ground2",[240,380],[0,0],mod)
        parent.Set_object("d_g04ground3","d_saguaro_ground2",[240,360],[0,0],mod)
        parent.Set_object("d_g04ground4","d_saguaro_ground2",[240,340],[0,0],mod)

        #2) add mountains
        sizes = [[197,81],[179,75],[221,81],[221,81]]
        mx = []
        for i in range(4):
            x1 = random.randint(250,765)
            ok = random.randint(0,2)
            for ccx in mx:
                if abs(x1-ccx) < 250:
                    ok = 0
            if ok:
                mx.append(x1)
        for mmx in mx:
            i = random.randint(0,3)
            x = mmx
            scale = 0.4+(random.randint(0,3)*0.2)
            mod = {"S":scale,"A":0,"I":0,"T":70}            
            if random.randint(0,1):
                x += int(sizes[i][0]*scale)
                mod = {"S":scale,"A":0,"I":1,"T":70}            
            parent.Set_object("d_g04mount%i"%mmx,"d_saguaro_m%i"%(i+1),[x,340],[0,sizes[i][1]],mod)

        #3) find the positions
        pos3,y = [],403
        while pos3 == []:
            x1 = random.randint(280,735)
            x2 = random.randint(280,735)
            x3 = random.randint(280,735)
            if abs(x1-x2) > 100 and abs(x1-x3) > 100 and abs(x2-x3) > 100:
                pos3 = [[x1,y],[x2,y],[x3,y]]
                pos3.sort()
        self.parent.pos3 = pos3[:]

        #5) add some cactus
        cx = []
        while cx == []:
            for i in range(6):
                x1 = random.randint(250,765)
                ok = 0
                if abs(x1-pos3[0][0]) > 50 and abs(x1-pos3[1][0]) > 50 and abs(x1-pos3[2][0]) > 50:
                    ok = 1
                for ccx in cx:
                    if abs(x1-ccx) < 50:
                        ok = 0
                if ok:
                    cx.append(x1)

        for cxx in cx:
            scale = 0.3+random.randint(0,3)*0.07
            mod = {"S":scale,"A":0,"I":0,"T":0}
            h = random.randint(0,int(100*scale))
            parent.Set_object("d_g04cactusa_"+repr(cxx),"d_saguaro_cactustrunk1",[cxx,403+h],[17,203],mod)
            realsize = (200*scale)-h
            if realsize-20 > 20:
                if random.randint(0,1):
                    hh = random.randint(20,realsize-20)
                    parent.Set_object("d_g04cactusb_"+repr(cxx),"d_saguaro_cactusbranch1",[cxx+int(15*scale),403-hh],[5,50],mod)
                if random.randint(0,1):
                    hh = random.randint(20,realsize-20)
                    mod = {"S":scale,"A":0,"I":1,"T":0}
                    parent.Set_object("d_g04cactusc_"+repr(cxx),"d_saguaro_cactusbranch1",[cxx-int(15*scale),403-hh],[5,50],mod)

        #6) cache
        mod = {"S":1,"A":0,"I":0,"T":0}
        parent.Set_object("d_g04bg","d_saguaro_cache",[0,0],[0,0],mod)

