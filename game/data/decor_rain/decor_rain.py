import random
from math import *
import time

class Decor:

    NAME="rain"
    IMAGES=["cache.png","drop.png","cloud.png","flash.png"]
    RARETY=2#the highest, the rarer
    #X.png of decor named Y will be named d_Y_X

    def __init__(self,parent):
        self.parent = parent

    def Load(self,style_cl,style_rs):
        parent = self.parent
        seed_continuity = random.random()
        random.seed(style_rs)
        d = self.parent.display
        mod = {"S":1,"A":0,"I":0,"T":0}
        parent.Set_object("d_g03bg1","bkgd_%i"%style_cl,[0,0],[0,0],mod)

        #1) get the three position areas
        pos3 = []
        x0,y0 = 0,398
        pos3.append([random.randint(290,370),396])
        if random.randint(0,1):
            pos3.append([random.randint(410,490),396])
            x0 = random.randint(550,590)
        else:
            x0 = random.randint(430,470)
            pos3.append([random.randint(530,610),396])
        pos3.append([random.randint(650,730),396])

        self.parent.pos3 = pos3[:]

        angle = random.randint(0,2)-1
        self.angle = angle

        a1 = [{"type":"move","x,y":[0,5]},{"type":"loop"}]
        parent.Set_motion("drop_fall",a1)
        if angle:
            a2 = [{"type":"move","x,y":[angle,0]},{"type":"loop"}]
            parent.Set_motion("drop_dev",a2)
            a4 = [{"type":"move","x,y":[angle,0]},{"type":"wait","time":1},{"type":"loop"}]
            parent.Set_motion("cloud_dev",a4)
        a3 = [{"type":"wait","time":10},{"type":"fct","fct":self.checkdrop},{"type":"loop"}]
        parent.Set_motion("drop_check",a3)
        a5 = [{"type":"random_wait","time1":1000,"time2":5000},
              {"type":"amove","x,y":[0,0]},
              {"type":"wait","time":2},
              {"type":"amove","x,y":[-1000,0]},
              {"type":"wait","time":1},
              {"type":"amove","x,y":[0,0]},
              {"type":"wait","time":1},
              {"type":"amove","x,y":[-1000,0]},
              {"type":"loop"}]
        parent.Set_motion("flash",a5)

        #flash
        self.parent.Set_object("d_g03_flash","d_rain_flash",[-1000,0],[0,0],{})
        parent.motions["flash"].list_object.append("d_g03_flash")

        #create all the cloud object
        nn = random.randint(3,6)+random.randint(0,3)+random.randint(0,3)
        for i in range(nn):
            scale = 0.5+(0.05*random.randint(0,4))
            mod = {"S":scale,"A":0,"I":0,"T":0}
            x,y = random.randint(240,750),random.randint(0,80)+random.randint(0,20)+random.randint(0,20)
            v = random.randint(0,1)
            self.parent.Set_object("d_g03_cloud_%i"%i,"d_rain_cloud",[x,y],[0,0],mod)
            if angle and v:
                parent.motions["cloud_dev"].list_object.append("d_g03_cloud_%i"%i)

        #create all the drops object
        ang = 45*angle
        sizes = [0,0,0,0,0,1,1,1,1,1,2,2,2,2,3,3,3,4,4,5]
        for i in range(200):
            x,y = random.randint(240,750),random.randint(0,420)
            size = sizes[i%len(sizes)]
            if size >= 2:
                y = (y*size)-(420*(size-1))
            if angle == 1:
                x = random.randint(0,750)
            if angle == -1:
                x = random.randint(240,1000)
            scale = 0.09+(0.02*size)
            mod = {"S":scale,"A":ang,"I":0,"T":0}
            self.parent.Set_object("d_g03_drop_%i"%i,"d_rain_drop",[x,y],[0,0],mod)
            parent.motions["drop_fall"].param_object["d_g03_drop_%i"%i] = {"ampl":1+size}
            parent.motions["drop_fall"].list_object.append("d_g03_drop_%i"%i)
            if angle:
                parent.motions["drop_dev"].param_object["d_g03_drop_%i"%i] = {"ampl":1+(size*size)}
                parent.motions["drop_dev"].list_object.append("d_g03_drop_%i"%i)

        #1) all drop are linked to the same motion
        #   object "ampl" is used to increase the drop speed
        #2) there is another motion using "fct" adn "wait", not linked to any object, that check regularly all the drops to see if y > 420 and put them back on top

        mod = {"S":1,"A":0,"I":0,"T":0}
        parent.Set_object("d_g03bg","d_rain_cache",[0,0],[0,0],mod)


    def checkdrop(self,object_name):
        for ok in self.parent.decor_obj:
            if self.angle and ok.startswith("d_g03_cloud"):
                if self.parent.decor_obj[ok].position[0] < 150:
                    self.parent.decor_obj[ok].Apply_motion_relativemove([700,0],{})
                if self.parent.decor_obj[ok].position[0] > 800:
                    self.parent.decor_obj[ok].Apply_motion_relativemove([-700,0],{})
            if ok.startswith("d_g03_drop") and self.parent.decor_obj[ok].position[1] > 420:
                x,y = random.randint(240,770),random.randint(0,400)-400
                if self.angle == 1:
                    x = random.randint(0,760)
                if self.angle == -1:
                    x = random.randint(240,1000)
                self.parent.decor_obj[ok].Apply_motion_absolutemove([x,y],{})
