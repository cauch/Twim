import random
from math import *
import time

class Decor:

    NAME="clearforest"
    IMAGES=["cache.png","branch.png","trunk.png","leaves5.png","leaf.png"]
    RARETY=1#the highest, the rarer
    #X.png of decor named Y will be named d_Y_X

    def __init__(self,parent):
        self.parent = parent

    def AddABranch(self,pos,scale,angle,top=0):
        leave_scale = 0.35
        x1,y1 = pos[0],pos[1]
        x2,y2 = x1-(scale*310*sin(pi*angle/(4*180))),y1-(scale*310*cos(pi*angle/(4*180)))
        if top:
            mod = {"S":leave_scale,"A":angle,"I":random.randint(0,1),"T":0}
            self.parent.Set_object("d_g02leaB_%f_%f"%(x2,y2),"d_clearforest_leaves5",[x2,y2],[95,200],mod)
        for pp in [0.2,0.6]:#[0.05,0.25,0.45,0.65]:
            pp1 = pp+(random.random()*0.05)
            pp2 = pp+(random.random()*0.05)
            cx1 = x2+pp1*(x1-x2)
            cy1 = y2+pp1*(y1-y2)
            cx2 = x2+pp2*(x1-x2)
            cy2 = y2+pp2*(y1-y2)
            ca1 = angle-360+random.randint(0,100)
            ca2 = angle+360-random.randint(0,100)
            ls1 = leave_scale+(random.random()*0.1)
            ls2 = leave_scale+(random.random()*0.1)
            mod = {"S":ls1,"A":ca1,"I":random.randint(0,1),"T":0}
            self.parent.Set_object("d_g02leaB_%f_%f_%i"%(cx1,cy1,ca1),"d_clearforest_leaves5",[cx1,cy1],[95,200],mod)
            mod = {"S":ls2,"A":ca2,"I":random.randint(0,1),"T":0}
            self.parent.Set_object("d_g02leaB_%f_%f_%i"%(cx2,cy2,ca2),"d_clearforest_leaves5",[cx2,cy2],[95,200],mod)
        mod = {"S":scale,"A":angle,"I":random.randint(0,1),"T":0}
        self.parent.Set_object("d_g02br_%f_%f_%i"%(x1,y1,angle),"d_clearforest_branch",[x1,y1],[10,320],mod)
        return x2,y2

    def Load(self,style_cl,style_rs):
        parent = self.parent
        seed_continuity = random.random()
        random.seed(style_rs)
        d = self.parent.display
        mod = {"S":1,"A":0,"I":0,"T":0}
        parent.Set_object("d_g02bg1","bkgd_%i"%style_cl,[0,0],[0,0],mod)
        
        #1) get the three position areas
        pos3 = []
        x0,y0 = 0,414
        pos3.append([random.randint(290,370),y0])
        if random.randint(0,1):
            pos3.append([random.randint(410,490),y0])
            x0 = random.randint(550,590)
        else:
            x0 = random.randint(430,470)
            pos3.append([random.randint(530,610),y0])
        pos3.append([random.randint(650,730),y0])

        self.parent.pos3 = pos3[:]

        #2) trees: trunk and leaves on the trunk
        y1 = y0-100-random.randint(0,100)
        y2 = y0-100-random.randint(0,200)

        scale = (4+random.randint(0,4))/8.
        scale = 0.5
        yLs = [y1,y2]
        for i in range(4):
            if i > 1 and random.randint(0,1):
                continue
            angleL = -360+100+random.randint(0,180)
            dx = 5
            if random.randint(0,1):
                dx = -5
                angleL = 360-100-random.randint(0,180)
            scaleL = random.randint(10,25)*2./100.
            invertL = random.randint(0,1)
            mod = {"S":scaleL,"A":angleL,"I":invertL,"T":0}
            yL = y0-50-random.randint(0,400)
            wd = 0
            while [x for x in yLs if abs(x-yL)<50]:
                yL = y0-50-random.randint(0,300)
                wd += 1
                if wd > 10:
                    break
            if wd > 10:
                continue
            yLs.append(yL)
            parent.Set_object("d_g02leaTr"+repr(i),"d_clearforest_leaves5",[x0+dx,yL],[95,200],mod)
        mod = {"S":scale,"A":0,"I":0,"T":0}
        parent.Set_object("d_g02trunk","d_clearforest_trunk",[x0,y0],[40,750],mod)

        #3) trees: branches
        angle1 = -17+90+(random.randint(0,3)*45)
        x11,y11 = self.AddABranch([x0,y1],scale,angle1,0)
        if y11 > 10:
            angle11 = angle1+90+(random.randint(0,3)*15)
            angle12 = angle1-90-(random.randint(0,3)*15)
            x11t,y11t = self.AddABranch([x11,y11],scale*0.5,angle11,1)
            x12t,y12t = self.AddABranch([x11,y11],scale*0.5,angle12,1)

        angle2 = -360+90+37+(random.randint(0,3)*45)
        x21,y21 = self.AddABranch([x0,y2],scale,angle2,0)
        if y21 > 10:
            angle21 = angle2+90+(random.randint(0,3)*15)
            angle22 = angle2-90-(random.randint(0,3)*15)
            x21t,y21t = self.AddABranch([x21,y21],scale*0.5,angle21,1)
            x22t,y22t = self.AddABranch([x21,y21],scale*0.5,angle22,1)

        #4) lateral branches
        if random.randint(0,2) == 0 or 1:
            if x0 > 500:
                angle = random.randint(20,70)*-4
                scale = 0.35+(0.05*random.randint(0,4))
                x11,y11 = self.AddABranch([250,random.randint(150,270)],scale,angle,1)
            else:
                angle = random.randint(20,70)*4
                scale = 0.35+(0.05*random.randint(0,4))
                x11,y11 = self.AddABranch([775,random.randint(150,270)],scale,angle,1)

        random.seed(seed_continuity)

        #5) falling leaf
        a1 = [{"type":"move","x,y":[0,3]},{"type":"loop"}]
        parent.Set_motion("leaf_fall_down",a1)
        a2 = [{"type":"mod","mod":{"A":14}},{"type":"loop"}]
        parent.Set_motion("leaf_fall_rot",a2)

        for ii in range(10):
            parent.Set_object("d_g03fl%i"%ii,"d_clearforest_leaf",[-500,0],[0,0],{"S":0.5,"A":random.randint(0,10)*60})
            parent.motions["leaf_fall_rot"].list_object.append("d_g03fl%i"%ii)
            parent.motions["leaf_fall_down"].list_object.append("d_g03fl%i"%ii)
            self.reset_falling_leaf("d_g03fl%i"%ii)

        #6) cache
        mod = {"S":1,"A":0,"I":0,"T":0}
        parent.Set_object("d_g02bg","d_clearforest_cache",[0,0],[0,0],mod)

    def reset_falling_leaf(self,no):
        parent = self.parent
        i = 0
        cr = "leaf_fall_lat%i"%i
        while cr in parent.motions.keys():
            i += 1
            cr = "leaf_fall_lat%i"%i
        a3 = [{"type":"random_wait","time1":100,"time2":500},
              {"type":"amove","x,y":[random.randint(300,799),random.randint(0,300)-300]}]
        x = 0
        for i in range(300):
            dx = 5*(random.random()-0.5)
            x = (dx+x+x+x)/4
            a3.append({"type":"move","x,y":[x,0]})
        a3.append({"type":"fct","fct":self.reset_falling_leaf})

        parent.Set_motion(cr,a3)
        parent.motions[cr].list_object.append(no)

