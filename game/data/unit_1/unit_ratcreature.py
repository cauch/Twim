import random
from math import *
import time

class Unit:

    NAME="ratcreature"
    RARETY=0
    TYPES=["ANI_LOCAL","ANI_MOVE"]
    TYPE=""
    IMAGES = ["ratcreature0.png"]

    def __init__(self,parent):
        self.parent = parent
        #parent can be twim_object or twim_world
        #depending on the function called

    def Load(self,i,o,pos):
        parent = self.parent
        mod = {"S":1,"A":0,"I":o,"T":0}
        parent.Set_object("u"+repr(i)+"_ratcreature"+repr(o),"u_ratcreature_ratcreature0",[pos[0],pos[1]],[25,93],mod)
            

    def Describe(self,info):
        text = "$b%s$e, a stupid stupid rat creature$n"%info["name"]
        text += "$n"
        t = ""
        for i in range(6):
            if info["statusH"][i] != 0:
                t += "$%i%i$e"%(i+1,info["statusH"][i])
        text += t+"$n$n"
        #sp = info.get("spells",[])
        return text

    def Fill(self,unit,room):
        w = room.world


