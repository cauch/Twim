import random
from math import *
import time

class Decor:

    NAME="plains"
    IMAGES=["bkgd.png","grass.png"]
    RARETY=1#the highest, the rarer
    #X.png of decor named Y will be named d_Y_X

    def __init__(self,parent):
        self.parent = parent

    def Load(self,style_cl,style_rs):
        parent = self.parent
        seed_continuity = random.random()
        random.seed(style_rs)
        d = self.parent.display
        parent.Set_object("d_g01bg1","bkgd_%i"%style_cl,[0,0],[0,0],{})

        #floor = [
        #    [247, 337],[260, 333],[275, 333],[292, 333],[305, 333],[319, 336],[333, 338],[345, 342],
        #    [354, 347],[369, 351],[375, 360],[388, 365],[395, 370],[406, 367],[422, 367],[435, 367],
        #    [444, 367],[455, 365],[467, 365],[480, 365],[497, 366],[510, 367],[534, 369],[548, 368],
        #    [560, 371],[574, 372],[587, 376],[595, 379],[606, 374],[618, 375],[633, 370],[645, 369],
        #    [664, 368],[683, 364],[705, 364],[720, 363],[735, 362],[758, 362],[777, 361],[787, 363]]

        #1) get the three position areas
        pos3 = []

        #point1
        point_i = random.randint(0,3)
        point = [[260, 340],[272, 337],[297, 337],[322, 340],[344, 344]][point_i:point_i+2]
        rx = random.random()
        pos3.append([point[0][0]+int(rx*(point[1][0]-point[0][0])),point[0][1]+int(rx*(point[1][1]-point[0][1]))])
        #point2
        point_i = random.randint(0,3)
        point = [[443, 366],[467, 365],[493, 364],[524, 367],[551, 371]][point_i:point_i+2]
        rx = random.random()
        pos3.append([point[0][0]+int(rx*(point[1][0]-point[0][0])),point[0][1]+int(rx*(point[1][1]-point[0][1]))])
        #point3
        point_i = random.randint(0,3)
        point = [ [628, 372],[658, 367],[688, 363],[722, 361],[756, 360]  ][point_i:point_i+2]
        rx = random.random()
        pos3.append([point[0][0]+int(rx*(point[1][0]-point[0][0])),point[0][1]+int(rx*(point[1][1]-point[0][1]))])

        self.parent.pos3 = pos3[:]

        #2) put the grass patches

        floor = [
            [242, 336],[269, 332],[297, 331],[326, 335],[356, 343],[375, 353],[392, 367],[420, 364],
            [447, 362],[479, 362],[508, 360],[531, 364],[559, 370],[578, 372],[596, 374],[618, 371],
            [646, 364],[671, 362],[698, 358],[722, 359],[746, 359],[776, 359],
        ]
        
        floorp = [floor[i-1]+floor[i] for i in range(1,len(floor))]

        #2.1) position of grass patches
        startagain = 1
        lt = []
        while startagain:
            lt = []
            i1 = random.randint(10,16)
            while i1 > 0:
                pos = floorp[random.randint(0,len(floorp)-1)]
                p1,p2 = pos[:2],pos[2:]
                rx = random.random()
                x,y = p1[0]+int(rx*(p2[0]-p1[0])),p1[1]+int(rx*(p2[1]-p1[1]))

                #not close to a pos3
                for j in pos3:
                    if abs(x-j[0]) < 30:
                        x=0
                        break
                if x:
                    i1 += -1
                    lt.append([x,y])

            startagain = 0
            #not close to another patch
            for i in range(len(lt)):
                for j in range(i+1,len(lt)):
                    if abs(lt[i][0]-lt[j][0]) < 7:#40:
                        startagain=1
                        break
        
        #2.2) create patches
        for l in lt:
            i3 = random.randint(2,4)
            da = 90/i3
            for i in range(i3):
                angle = (i*da)+random.randint(0,da)
                scale = random.randint(10,18)*2./100.
                invert = (angle+random.randint(0,10) >= 40)
                angle = angle-45
                mod = {"S":scale,"A":angle*4,"I":invert,"T":0}
                parent.Set_object("d_g01_grass_"+repr(l)+repr(i),"d_plains_grass",[l[0],l[1]+3],[5,110],mod)
                #parent.decor_obj["g01"+repr(l)+repr(i)].angle = angle

        random.seed(seed_continuity)

        #3) motion
        mod = {"S":1,"A":0,"I":0,"T":0}
        parent.Set_object("d_g01bg2","d_plains_bkgd",[0,0],[0,0],mod)

        a1 = [{"type":"random_wait","time1":15,"time2":30},{"type":"fct","fct":self.createwindmotion},{"type":"loop"}]
        a2 = [{"type":"random_wait","time1":0,"time2":25},{"type":"fct","fct":self.createwindmotion},{"type":"loop"}]
        parent.Set_motion("wind_main1",a1)
        parent.Set_motion("wind_main2",a2)

    def createwindmotion(self,object_name):
        parent = self.parent
        cr = "wind1"
        if "wind1" in parent.motions.keys():
            cr = "wind2"
            if "wind2" in parent.motions.keys():
                cr = "wind3"
                if "wind3" in parent.motions.keys():
                    return

        a = []
        #pattern = [-1,-2,-3,-4,-5,-6,-6,-7,-7,-7,-8,-8,-8,-8,-7,-7,-7,-6,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,6,7,7,7,8,8,8,8,7,7,7,6,6,5,4,3,2,1,0]
        pattern = [-1,-1,-1,-1,-1,-1,0,-1,0,0,-1,0,0,0,1,0,0,1,0,1,1,1,1,1,1,1,1,1,1,0,1,0,0,1,0,0,0,-1,0,0,-1,0,-1,-1,-1,-1,-1,-1,0-1,0,0,1,0,1,1]
        for i in range(len(pattern)):
            if pattern[i] == 0:
                a.append({"type":"empty"})
            else:
                mod = {"A":pattern[i]}
                a.append({"type":"mod","mod":mod})

        parent.Set_motion(cr,a)
        parent.motions[cr].param_global = {"intensity":random.randint(1,4)}
        lo = []
        for ok in parent.decor_obj:
            if "grass" not in ok:
                continue
            x = (parent.decor_obj[ok].rect.left)/-10
            parent.motions[cr].param_object[ok] = {"delay":x}
            lo.append(ok)
        parent.motions[cr].list_object = lo[:]
        
