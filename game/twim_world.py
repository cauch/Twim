#    This file is part of TWIM.
#
#    TWIM is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    TWIM is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with TWIM.  If not, see <http://www.gnu.org/licenses/>.

"""
Classes of the objects of the gameplay (world, room, spell, unit)
This is used by
twim_server
"""

import random
import os
import imp
from math import *
from twim_spell import *
from twim_nature import *

def Create_name(li=[]):
    """
    Function that create a random name
    """
    i = 0
    if li:
        i = li[random.randint(0,len(li)-1)]
    else:
        i = random.randint(1,9)
    t = ''
    if i <= 1:
        i1,i2,i3,i4,i5,i6,i7,i8,i9 = 10,10,20,6,3,1,3,7,4
        c,v = 'rtpsdfglmcvbn','aeiuo'
        c2 = ['ch','bl','cl','tr','fl','dr']
        syl1 = ['en','on','ou','in','an']
        e,e2 = ['','n','m',''],['','e','i','a','u','o']
    elif i == 2:
        i1,i2,i3,i4,i5,i6,i7,i8,i9 = 5,5,5,6,3,1,3,7,4
        c,v = 'zrtpqsdfghjklmwxcvbnrtkcgjwxrtkq','eeeaauiio'
        c2 = ['zl','zr','br','cr','pr','tr','fr','st','gr']
        syl1 = ['ou','ow','aw','ew','on','un','uw','ew']
        e,e2 = ['s','z','t','r','h','x'],['ez','az','ex','es','as','eg','aw','ew','ir','er','et','ax']
    elif i == 3:
        i1,i2,i3,i4,i5,i6,i7,i8,i9 = 10,10,20,6,3,1,3,6,3
        c,v = 'pzdfghjlmwvbn','aaaeeei'
        c2 = ['mn','fl','dl','gn','gl','bl','ph']
        syl1 = ['en','on','en','in','an','an','en','an','on','un']
        e,e2 = ['l','l','m',''],['im','im','im','il','il','il','il','el','ig','em']
    elif i == 4:
        i1,i2,i3,i4,i5,i6,i7,i8,i9 = 10,10,20,6,3,1,4,5,1
        c,v = 'szfgjktgwhsft','eeeuuuiio'
        c2 = ['ph','st']
        syl1 = ['we','wi','wo']
        e,e2 = ['z','z','z','g'],['ez','eg']
    elif i == 5:
        i1,i2,i3,i4,i5,i6,i7,i8,i9 = 10,10,20,6,4,2,7,8,3
        c,v = 'wjgdgtrcpqmngdwjtrcpwgzrtpqsdfghjklmwxcvbn','aaeeeuiiio'
        c2 = ['ch','cr','fl']
        syl1 = ['wa','wi']
        e,e2 = ['g','g','g','g','g','g','g','g','g','g','k','k','k','k','k','k','m','n','r','t','j','','f'],['ewig','edig','ig','ejig','emig','ewig','ewig','etrag','etrak']
    elif i == 6:
        i1,i2,i3,i4,i5,i6,i7,i8,i9 = 6,6,12,6,5,2,5,10,1
        c,v = 'zrtpsdhjkkkklmwxcvbnwwwhhhhllllmmmmppppxxxxxzzzzzz','aaaaiiiieuuooo'
        c2 = ['ch','xh','-','-']
        syl1 = ['on','en','-n','-z']
        e,e2 = ['a','i','o','','a','i'],['eni','ena','eno','ewa']
    elif i == 7:
        i1,i2,i3,i4,i5,i6,i7,i8,i9 = 8,3,10,6,4,1,5,11,3
        c,v = 'zrtpqsdfghjklmwxcvbnrtckxcrkk','aeuioyoeoeoaiieoeooo'
        c2 = ['cr','gr','tr','cr','cr','kr','cr','kl','gr']
        syl1 = ['on','en','oo','on']
        e,e2 = [],['ogrok','ogrek','orrek','ognork','oglok','egrok','onork','ork','ogrok','ogrok']
    elif i == 8:
        i1,i2,i3,i4,i5,i6,i7,i8,i9 = 10,10,10,4,5,2,3,10,1
        c,v = 'rtpsdfghjlmxcbnrtupsdflmcbnupslmcbncntmlpns','aeiuoeia'
        c2 = ['cr','tr','bl','gl','ph','ph','ph','fl','pl']
        syl1 = ['ae','ae','um','on','en','ia']
        e,e2 = ['','s','i','s','',''],['us','um','us','is','ae']
    elif i == 9:
        i1,i2,i3,i4,i5,i6,i7,i8,i9 = 8,0,10,0,5,2,4,9,1
        c,v = 'bdfgklmnprstvzbdfgklmnprstvzxcjh','ooooaaaiiiuuue'
        c2 = []
        syl1 = ['wo','wa','wi','wu','wo','wa','wi']
        e,e2 = ['','n','m','','',''],['o','o','e','a','u']
    syl = []
    for l in syl1:
        a = c[int(len(c)*random.random())]
        syl.append(a+l)
    for l in range(i1):
        e.append(c[int(len(c)*random.random())]+e2[int(len(e2)*random.random())])
    for l in range(i2):
        e.append(c2[int(len(c2)*random.random())]+e2[int(len(e2)*random.random())])
    for l in range(i3):
        syl.append(c[int(len(c)*random.random())]+v[int(len(v)*random.random())])
    for l in range(i4):
        syl.append(c2[int(len(c2)*random.random())]+v[int(len(v)*random.random())])
    for l in range(int(random.random()*i5)+i6):
        t = t+syl[int(random.random()*len(syl))]
    t = t+e[int(random.random()*len(e))]
    if (len(t) < i7) or (len(t) > i8+int(random.random()*i9)) or (t[0]=="-"): t = Create_name()
    return t

class Unit:
    def __init__(self,w):
        self.x = 0
        self.y = 0
        self.name = ""
        self.what = ""
        self.world = w
        self.room_keyname = "0_0"
        self.statusS = [0,0,0,0,0,0]
        self.statusH = [0,0,0,0,0,0]
        self.player_number = -1
        self.spells = []
        self.spell_delays = {}
        self.knowledge = []
        self.level = 0
        self.u = ""

    def __getstate__(self):
        """
        Veto attributes that should not be saved in files
        """
        pickle_veto = ["world","u"]
        return {k: v for k, v in self.__dict__.iteritems() if k not in pickle_veto}

    def Copy(self):
        u = Unit(self.world)
        u.x = self.x
        u.y = self.y
        u.name = self.name
        u.what = self.what
        u.room_keyname = self.room_keyname
        u.statusS = self.statusS[:]
        u.statusH = self.statusH[:]
        u.player_number = self.player_number
        u.spells = self.spells[:]
        for k in self.spell_delays:
            u.spell_delays[k] = self.spell_delays[k]
        return u

    def Move(self,r):
        self.x = r.pos[0]
        self.y = r.pos[1]
        self.room_keyname = r.keyname

    def Get_info(self):
        di = {}
        di["statusS"] = self.statusS
        di["statusH"] = self.statusH
        di["what"] = self.what
        di["name"] = self.name
        if self.spells:
            di["spells"] = self.spells
        if self.spell_delays:
            di["spell_delays"] = self.spell_delays
        return di

    def Clean_spelldelays(self,server):
        k = self.spell_delays.keys()
        for spn in k:
            time_now = server.time
            time_back = self.spell_delays[spn]
            if time_back < time_now:
                del self.spell_delays[spn]

    def Get_spells(self):
        if "spells2" not in dir(self):
            self.spells2 = []
        if self.spells2:
            return
        level = self.level
        lk = self.world.spells.keys()
        random.shuffle(lk)
        for k in lk:
            sp = self.world.spells[k]
            if sp.level == -1:
                continue
            if random.randint(0,11-sp.level+level) == 0:
                continue
            if random.randint(0,5) == 0:
                continue
            self.spells2.append(k)

    def Get_affordable_spells(self,what):
        spells = []
        self.Get_spells()
        for k in self.spells2:
            sp = self.world.spells[k]
            if sp.what != what and what != -1:
                continue
            if sp.level == -1:
                continue
            vr = []
            if what == 5:
                vr = [max(-1,self.statusS[i]-sp.val1[i]) for i in range(6) if sp.val1[i] != 0]
            elif what == 3:
                vr = [max(-1,self.statusS[i]-sp.val1[i]) for i in range(6) if sp.val1[i] != 0]
            else:
                print "ERROR, not taken into account"
            if vr.count(-1):
                continue
            spells.append(k)
        return spells

    def Fill(self,room):
        #this is the first time, overwrite itself
        if self.u == "":
            self.u = self.world.server.modules_unit.get(self.what)
        if self.u:
            self.u(None).Fill(self,room)

    def Fill_default(self,room):
        self.u(None)
        if self.world.server.modules_unit[self.what].TYPE == "CONT_SP_O":
            while len(self.spells) < 10:
                sp = self.world.Find_spell([3,4,5,6,7],[0,1,2,3],room.style[0])
                if sp:
                    if sp.used == 0:
                        sp.used = 1
                        self.spells.append(sp.name)
                        print sp.name,sp.what,"lev:",sp.level



class World:
    def __init__(self,server):
        """
        Main classe, that contains every objects of the world.
        
        argument:
           server, the server object
        """
        #server
        self.server = server
        #spell stuffs
        self.spells = {}      #dictionnary of spells: name <-> spell
        self.ispells = {}     #inverted dictionnary: type of spell (what) <-> list of spells of this type
        #room stuffs
        self.rooms = {}       #list of the rooms, key is "x_y"
        self.irooms = {}      #dictionnary "name" -> "x_y"
        self.list_to_walk = []#list of rooms that does not exist yet but some other rooms point to them
        #unit stuffs (player is an unit, with player_number != -1)
        self.units = {}       #list of units
        self.iplayers = {}    #dictionnary "player_number" -> "player_name"

        self.structure_unit = {} #say, for example, this unit is high level and in this color
                                 #unit_structure[name_of_unit] = [type,[colors],level,rarety]
        self.structure_unit_type = {}

        self.structure_decor = [] #return the list of available decor for a given color

        self.nature = Nature(self)


    def __getstate__(self):
        """
        Veto attributes that should not be saved in files
        """
        pickle_veto = ["server"]
        return {k: v for k, v in self.__dict__.iteritems() if k not in pickle_veto}

    def Get_unit_fromstructure(self,typ,level=-1,cl=-1):
        sut = []
        really_nothing = 1
        for mod in self.structure_unit_type[typ]:
            if cl != -1 and cl not in self.structure_unit[mod][1]:
                continue
            if level != -1 and self.structure_unit[mod][2] < level:
                continue
            really_nothing = 0
            if random.randint(0,10+self.structure_unit[mod][3]) <= 10:
                sut.append(mod)
        if really_nothing:
            return None
        if not sut:
            return self.Get_unit_fromstructure(typ,level,cl)
        return sut[random.randint(0,len(sut)-1)]
            

    def Create_structures(self):
        """
        Set the characteristics of the unit/decor modules
        
        """
        #1) decor

        #class the rarety
        #if you have A=1, B=1, C=1, D=2, E=10
        #it means that the probability to get A is ( 1./1 ) / ( 1./1 + 1./1 + 1./1 + 1./2 + 1./10 )

        rarety_values = {}
        for mo in self.server.modules_decor:
            rarety_values[mo] = 1./(self.server.modules_decor[mo](None).RARETY)
        total = sum(rarety_values.values())
        minimum = min(rarety_values.values())*1./total
        rarety_prob = {l:(rarety_values[l]*1./total)*6./minimum for l in rarety_values.keys()}
        rlist = []
        for mo in rarety_prob:
            rlist += [mo]*int(rarety_prob[mo])
        random.shuffle(rlist)
        i = len(rlist)/6
        self.structure_decor = [rlist[0:i],
                                rlist[i:i*2],
                                rlist[i*2:i*3],
                                rlist[i*3:i*4],
                                rlist[i*4:i*5],
                                rlist[i*5:i*6]
                               ]

        #2) unit
        #type of unit:
        #CONT_SP_O: container, object persistent, contain spell, don't need a spell
        #CONT_SP_L: container, object non persistent, contain spell, need a spell
        #CONT_CL_O: container, object persistent, contain color, don't need a spell
        #CONT_CL_L: container, object non persistent, contain color, need a spell
        #RESS_CL: color consommable, need a spell, can appear
        #RESS_SP: spell consommable, need a spell, can appear
        #ANI_LOCAL: don't move, attack and defend, can be killed, can appear 
        #ANI_MOVE: move, attack and defend, can be killed, can appear 
        #PLAYER: used by player

        types = {"CONT_SP_O":[],"CONT_SP_L":[],"CONT_CL_O":[],"CONT_CL_L":[],"RESS_CL":[],"RESS_SP":[],"ANI_LOCAL":[],"ANI_MOVE":[],"PLAYER":[]}
        for k in types:
            self.structure_unit_type[k] = [] 
        rarety = {}

        for mo in self.server.modules_unit:
            t = self.server.modules_unit[mo](None).TYPES
            rarety[mo] = self.server.modules_unit[mo](None).RARETY
            for tt in t:
                types[tt].append(mo)

        #shuffle them
        for ti in types:
            tp = types[ti][:]
            random.shuffle(tp)
            types[ti] = tp

        #take 1 player
        #take 1 cont_sp_o
        #take 1 ani_local
        #take 1 ani_move
        for l in ["PLAYER","CONT_SP_O","ANI_LOCAL","ANI_MOVE"]:
            mo = types[l][0]
            for tt in self.server.modules_unit[mo](None).TYPES:
                types[tt].remove(mo)
            self.server.modules_unit[mo].TYPE = l

        #now, fill randomly
        while types:
            k = types.keys()
            random.shuffle(k)
            ke = k[0]
            #if ke not in types:
            #    continue
            if types[ke]==[]:
                del types[ke]
                continue
            mo = types[ke][0]
            ct = self.server.modules_unit[mo](None).TYPES
            random.shuffle(ct)
            self.server.modules_unit[mo].TYPE = ct[0]
            for tt in ct:
                if tt in types:
                    types[tt].remove(mo)
                    if types[tt] == []:
                        del types[tt]

        #associate with colors

        for mo in self.server.modules_unit:
            colors = range(6)
            level = 0
            if self.server.modules_unit[mo](None).RARETY > 0:
                level = random.randint(0,10)
                random.shuffle(colors)
                colors = colors[:random.randint(1,5)]
            ti = self.server.modules_unit[mo].TYPE
            self.structure_unit[mo] = [ti,colors,level,rarety[mo]]
            self.structure_unit_type[self.server.modules_unit[mo](None).TYPE].append(mo)
    
    def Check_name_exists(self,name):
        if name in self.spells.keys():
            return 1
        if name in self.irooms.keys():
            return 1
        if name in self.units.keys():
            return 1
        return 0

    def Create_spells(self):
        """
        Generate random spells

        WIP
        """
        spell_needs = ["1_-1_0","2_-1_0"]
        # w_p_ccc we need a spell of what w of level p, with colors c,c and c (can be empty)
        spell_pop = []
        # population of spells: to avoid level in only one type, ...

        for i in range(200):
            spell_needs.append("")

        for i in range(len(spell_needs)):
            n = ""
            while self.Check_name_exists(n) or n=="" or (len(n) > (i/2)+5):
                n = Create_name()
            self.spells[n] = Spell(n,self)
            self.spells[n].Randomize(spell_needs[i])
            j = self.spells[n].what
            if j not in self.ispells:
                self.ispells[j] = []
            self.ispells[j].append(n)

        if "Command" in os.environ.get("TWIM_DEBUG",""):
            self.spells["attack"] = Spell("attack",self)
            self.spells["attack"].what = 5
            self.spells["attack"].val2 = [5,5,5,5,5,5]
            self.spells["cd"] = Spell("cd",self)
            self.spells["cd"].what = 2
            self.spells["man"] = Spell("man",self)
            self.spells["man"].what = 1

    def Find_spell(self,what,level,cl):
        """
        Return a spell from the list that satisfy the criteria

        argument:
           what: list of what values that the spell can have
           level: list of level values that the spell can have ([] if we don't care)
           cl: the color it can have (-1 if we don't care)
        """
        cwhat = 0
        if what:
            random.shuffle(what)
            cwhat = what[0]
        los = self.ispells[cwhat][:]
        random.shuffle(los)
        if level == []:
            level = range(10)
        tlos = los[:]
        for s in tlos:
            if [i for i in level if self.spells[s].level == i] == []:
                los.remove(s)
        random.shuffle(level)
        while los:
            for le in level:
                tlos = los[:]
                for s in tlos:
                    if self.spells[s].level != le:
                        continue
                    tc = self.spells[s].Test_color(cl)
                    if tc == 1:
                        los.remove(s)
                    elif tc == 0:
                        return self.spells[s]
        return None

    def Generate_room_around(self,n,number,radius):
        """
        Generate several rooms in a given region

        WIP
        """

        new_rooms = []

        #0) if nothing, start
        if self.rooms == {}:
            self.Generate_room(n)
            self.rooms[n].real_cl = [1,0,0,0,0,0]
            self.rooms[n].cl = [3,0,0,0,0,0]
            new_rooms.append(n)
        #1) remove the element that are anyway too far
        no_need_to_recheck_them = []
        x1,y1 = int(n.split("_")[0]),int(n.split("_")[1])
        for nn in self.list_to_walk:
            x2,y2 = int(nn.split("_")[0]),int(nn.split("_")[1])
            if ((x1-x2)**2) + ((y1-y2)**2) > radius**2:
                no_need_to_recheck_them.append(nn)
        #2) find in list_to_walk a room close to n
        for i in range(number):
            rw = [x for x in self.list_to_walk if x not in no_need_to_recheck_them]
            if len(rw) == 0:
                break
            p = rw[0]
            new_rooms.append(self.Generate_room(p))

        #4) uniformize the decor
        for i in range(int(len(new_rooms)*1.5)):
            rnr = random.randint(0,len(new_rooms)-1)
            nr = new_rooms[rnr]
            ro = self.rooms[nr]
            ns = []
            ns.append("%i_%i"%(ro.pos[0],ro.pos[1]-1))
            ns.append("%i_%i"%(ro.pos[0]+1,ro.pos[1]))
            ns.append("%i_%i"%(ro.pos[0],ro.pos[1]+1))
            ns.append("%i_%i"%(ro.pos[0]-1,ro.pos[1]))
            ns.append("%i_%i"%(ro.pos[0]-1,ro.pos[1]-1))
            ns.append("%i_%i"%(ro.pos[0]-1,ro.pos[1]+1))
            ns.append("%i_%i"%(ro.pos[0]+1,ro.pos[1]-1))
            ns.append("%i_%i"%(ro.pos[0]+1,ro.pos[1]+1))
            los = {}
            for nss in ns:
                if nss in self.rooms.keys():
                    if self.rooms[nss] == None:
                        continue
                    if self.rooms[nss].style[1] not in los:
                        los[self.rooms[nss].style[1]] = 0
                    los[self.rooms[nss].style[1]] += 1
            l1 = [[los[k],k] for k in los]
            l1.sort()
            ma = l1[-1][0]
            l2 = [k for k in los if los[k]==ma]
            ro.style[1] = l2[random.randint(0,len(l2)-1)]

        #5) fill the rooms
        for nr in new_rooms:
            self.rooms[nr].Fill()
    
    def Generate_room(self,np):
        """
        Generate a new room in position np

        argument:
           np: position in text, for example: "0_0"

        return:
           key of the newly generated room

        algorithm:
           if the room already exists, exit
           look around, and check if there is an accessible room
            if room[n] if None -> impossible to go out to there
            if there is (or will be) a room, then, you must go out to there
           based on that, choose the remaining exits randomly
           if there is not a lot of planned rooms, force to add additional exits

        example on how to use it:
        w = World()
        w.Generate_room_around("0_0",20,5)
        w.Print_rooms()
        n = w.rooms.keys()[-1]
        print "add around",n
        w.Generate_room_around(n,5,2)
        w.Print_rooms()

        WIP
        """
        p = [int(np.split("_")[0]),int(np.split("_")[1])]
        n0 = "%i_%i"%(p[0],p[1])
        if n0 in self.rooms.keys():
            #already done -> exit
            if np in self.list_to_walk:
                self.list_to_walk.remove(np)
            return []
        n1 = "%i_%i"%(p[0],p[1]-1)
        n2 = "%i_%i"%(p[0]+1,p[1])
        n3 = "%i_%i"%(p[0],p[1]+1)
        n4 = "%i_%i"%(p[0]-1,p[1])
        ns = [n1,n2,n3,n4]
        c = []
        for nn in ns:
            if nn not in self.rooms:
                if nn in self.list_to_walk:
                    c.append(1)
                else:
                    c.append(0)
            else:
                if self.rooms[nn]:
                    c.append(1)
                else:
                    c.append(2)
        if c == [2,2,2,2]:
            #surrended by None rooms -> no room
            self.rooms[n0] = None
            if np in self.list_to_walk:
                self.list_to_walk.remove(np)
            return
        self.rooms[n0] = Room(n0,self)
        self.irooms[self.rooms[n0].name] = n0
        foe = []#forced exits
        fow = []#forced walls
        for i in range(4):
            if c[i] == 1:
                foe.append(i)
            if c[i] == 2:
                fow.append(i)
        mb = []#maybe an exit
        for i in range(4):
            if i in fow:
                continue
            if i in foe:
                self.rooms[n0].exits.append(i)
            else:
                mb.append(i)
        random.shuffle(mb)
        ii = random.randint(1,4)-len(self.rooms[n0].exits)
        if len(self.list_to_walk) <= 2:
            #not a lot of planned rooms -> force at least one new exits
            if len(self.rooms[n0].exits) < 4:
                ii = random.randint(1,4-len(self.rooms[n0].exits))
            else:
                print "hm, spiral problem ?"
        lta = []#to add
        ltv = mb[:]#to not add
        if ii > 0:
            lta = mb[:ii]
            ltv = mb[ii:]
        for i in lta:
            self.rooms[n0].exits.append(i)
            self.list_to_walk.append(ns[i])
        for i in ltv:
            self.rooms[ns[i]] = None
        if np in self.list_to_walk:
            self.list_to_walk.remove(np)

        #colors
        all_cl = [0,0,0,0,0,0]
        for i in range(4):
            if c[i] == 1 and ns[i] in self.rooms:
                all_cl = [all_cl[j]+self.rooms[ns[i]].real_cl[j] for j in range(6)]
        if all_cl == [0,0,0,0,0,0]:
            all_cl = [1,0,0,0,0,0]
        norm = all_cl[0]+all_cl[1]+all_cl[2]+all_cl[3]+all_cl[4]+all_cl[5]
        if norm:
            all_cl = [[all_cl[i]*1./norm,i] for i in range(6)]
        else:
            all_cl = [[all_cl[i],i] for i in range(6)]
        #keep only the 3 highest
        all_cl.sort()
        ac = [0,0,0,0,0,0]
        ac[all_cl[-1][1]] = all_cl[-1][0]
        ac[all_cl[-2][1]] = all_cl[-2][0]
        ac[all_cl[-3][1]] = all_cl[-3][0]
        #add a possible deviation
        rd = random.randint(1,7)
        for i in range(rd):
            j = random.randint(0,5)
            if ac[j] != 0:
                ii = j+int(pow(-1,random.randint(1,2)))
                if ii < 0: ii = 0
                if ii > 5: ii = 5
                ac[ii] += 0.3
        norm = ac[0]+ac[1]+ac[2]+ac[3]+ac[4]+ac[5]
        if norm:
            ac = [ac[i]*1./norm for i in range(6)]
        #max 4 different things
        while ac.count(0) <= 2:
            ac[random.randint(0,5)] = 0
        #set real_cl
        self.rooms[n0].real_cl = ac[:]
        #print self.rooms[n0].real_cl
        #fill cl
        lac = []
        for i in range(6):
            for j in range(int(ac[i]*10)):
                lac.append(i)
        random.shuffle(lac)
        self.rooms[n0].cl = [0,0,0,0,0,0]
        #self.rooms[n0].cl = [random.randint(1,2),random.randint(1,2),0,0,0,0]
        if lac:
            self.rooms[n0].cl[lac[0]] += 1
            if len(lac) > 1:
                self.rooms[n0].cl[lac[1]] += 1
            if len(lac) > 2:
                self.rooms[n0].cl[lac[2]] += 1
            self.rooms[n0].style[0] = lac[0]
            pd = self.structure_decor[lac[0]]
            self.rooms[n0].style[1] = pd[random.randint(0,len(pd)-1)]
        return n0


    def Get_nextroom(self,n,di):
        if n not in self.rooms.keys():
            return None
        p = [int(n.split("_")[0]),int(n.split("_")[1])]
        nr = ""
        if di == 0:
            nr = "%i_%i"%(p[0],p[1]-1)
        elif di == 1:
            nr = "%i_%i"%(p[0]+1,p[1])
        elif di == 2:
            nr = "%i_%i"%(p[0],p[1]+1)
        elif di == 3:
            nr = "%i_%i"%(p[0]-1,p[1])
        if nr not in self.rooms.keys():
            return None
        else:
            return self.rooms[nr]
    
    def Print_rooms(self):
        """
        For debugging, print a map of the rooms (a room is a 3x3 with | and - for the doors)
        """
        ex,ey = [],[]
        for k in self.rooms:
            x = int(k.split("_")[0])
            y = int(k.split("_")[1])
            if ex == []:
                ex = [x,x]
            if ey == []:
                ey = [y,y]
            if x < ex[0]:
                ex[0] = x
            if y < ey[0]:
                ey[0] = y
            if x > ex[1]:
                ex[1] = x
            if y > ey[1]:
                ey[1] = y
        print ex,ey
        for iy in range(ey[0]-1,ey[1]+1):
            t1 = ""
            t2 = ""
            t3 = ""
            t4 = ""
            t5 = ""
            for ix in range(ex[0]-1,ex[1]+1):
                n = "%i_%i"%(ix,iy)
                if n not in self.rooms.keys():
                    t1 += "       "
                    t2 += "       "
                    t3 += "       "
                    t4 += "       "
                    t5 += "       "
                else:
                    if self.rooms[n]:
                        t3m = " "
                        if n == "0_0": t3m = "O"
                        if 0 in self.rooms[n].exits:
                            t1 += " xx|xx "
                        else:
                            t1 += " xxxxx "
                        if 1 in self.rooms[n].exits and 3 in self.rooms[n].exits:
                            t3 += " - %s - "%t3m
                        elif 1 in self.rooms[n].exits:
                            t3 += " x %s - "%t3m
                        elif 3 in self.rooms[n].exits:
                            t3 += " - %s x "%t3m
                        else:
                            t3 += " x %s x "%t3m
                        if 2 in self.rooms[n].exits:
                            t5 += " xx|xx "
                        else:
                            t5 += " xxxxx "
                        t2 += " x%i%i%ix "%(self.rooms[n].cl[0],self.rooms[n].cl[1],self.rooms[n].cl[2])
                        t4 += " x%i%i%ix "%(self.rooms[n].cl[3],self.rooms[n].cl[4],self.rooms[n].cl[5])
                    else:
                        t1 += " ..... "
                        t2 += " ..... "
                        t3 += " ..... "
                        t4 += " ..... "
                        t5 += " ..... "
            print " "*len(t1)
            print t1
            print t2
            print t3
            print t4
            print t5
            print " "*len(t1)

    def Link_playertoloaded(self,player_number,player_name):
        #we've loaded a file
        #the player_name is already there somewhere
        #maybe the number is not correct
        if player_name not in self.units.keys():
            self.Create_newplayer(player_number,player_name)
        else:
            self.units[player_name].player_number = player_number
            self.iplayers[player_number] = player_name

    def Create_newplayer(self,player_number,player_name):
        #print "new player",player_number
        if self.Check_name_exists(player_name):
            print "The name",player_name,"already exists"
            return
        x,y = 0,0
        while 1:
            ok =  1
            for i in self.units.keys():
                if self.units[i].player_number == -1:
                    continue
                if (x-self.units[i].x)**2 + (y-self.units[i].y)**2 < 100:
                    ok = 0
                    break
            if ok:
                break
            x,y = random.randint(-500,500),random.randint(-500,500)
        pn = player_name
        self.units[pn] = Unit(self)
        self.units[pn].player_number = player_number
        self.units[pn].name = pn
        self.units[pn].what = self.Get_unit_fromstructure("PLAYER")
        self.units[pn].statusS = [100,100,0,0,0,0]
        self.units[pn].statusH = [50,50,0,0,0,0]
        self.iplayers[player_number] = pn
        room_key = "%i_%i"%(x,y)
        self.rooms[room_key].Add_unit(pn)


class Room:
    def __init__(self,p,w):
        """
        Room
        """
        self.world = w
        self.style = [0,0,random.random()]
        #style of the room
        # 1st: color
        # 2nd: decor style
        # 3rd: random generator seed
        self.exits = []
        self.units = []
        self.cl = [0,0,0,0,0,0]     #cl used for the pay
        self.real_cl = [0,0,0,0,0,0]#cl used for the color propagation, normalized to 1
        for i in range(6):
            self.cl[i] = random.randint(0,4)
        self.pos = [int(p.split("_")[0]),int(p.split("_")[1])]
        self.keyname = p
        self.name = Create_name()
        while w.Check_name_exists(self.name):
            self.name = Create_name()

    def __getstate__(self):
        """
        Veto attributes that should not be saved in files
        """
        pickle_veto = ["world"]
        return {k: v for k, v in self.__dict__.iteritems() if k not in pickle_veto}

    def Create_unit(self,what):
        n = Create_name()
        while self.world.Check_name_exists(n):
            n = Create_name()
        self.world.units[n] = Unit(self.world)
        self.world.units[n].name = n
        self.world.units[n].what = what
        self.world.units[n].Fill(self)
        self.Add_unit(n)
        return n

    def Fill(self):
        if self.pos == [0,0]:
            #special: just 1 book
            un = self.world.Get_unit_fromstructure("CONT_SP_O")
            n = self.Create_unit(un)
            #test a new unit
            #n = self.Create_unit("ratcreature")
            #self.world.units[n].statusS = [0,0,0,0,0,0]
            #self.world.units[n].statusH = [1,0,0,0,0,0]
        else:
            if random.randint(0,3) == 0 or 1:
                un = self.world.Get_unit_fromstructure("ANI_LOCAL")
                n = self.Create_unit(un)
                self.world.units[n].statusS = [x*5 for x in self.cl]
                self.world.units[n].statusH = [x*2 for x in self.cl]
                lopc = [i for i in range(6) if self.real_cl[i] > 0.05]
                lopc += lopc[:]+lopc[:]+[random.randint(0,5),random.randint(0,5)]
                for i in range(5+random.randint(0,5)):
                    random.shuffle(lopc)
                    lli = lopc[0]
                    if self.world.units[n].statusH[lli] and random.randint(0,3)==0:
                        self.world.units[n].statusH[lli]+=1
                    if self.world.units[n].statusS[lli] and random.randint(0,2)==0:
                        self.world.units[n].statusS[lli]+=3
                    if self.world.units[n].statusS[lli]==0 and random.randint(0,5)==0:
                        self.world.units[n].statusS[lli] = 4+random.randint(0,4)
                #print self.cl,self.real_cl,self.world.units[n].statusS,self.world.units[n].statusH

    def Add_unit(self,unit_name):
        if unit_name not in self.units:
            if int(self.style[2]*100)%2:
                self.units.append(unit_name)
            else:
                self.units = [unit_name]+self.units
            u = self.world.units[unit_name]
            u.Move(self)

    def Remove_unit(self,unit_name):
        if unit_name in self.units:
            self.units.remove(unit_name)

    def Get_players(self):
        pl = []
        for u in self.units:
            if self.world.units[u].player_number != -1:
                pl.append(u)
        return pl

    def Get_info(self,player_name):
        di = {}
        di["style"] = self.style
        di["name"] = self.name
        #find the units around the player
        il,ir = -1,-1
        for i,u in enumerate(self.units):
            if u == player_name:
                il = i-1
                ir = i+1
        if ir >= len(self.units):
            ir = -1
        di_ul = {}
        di_ul["name"] = ""
        di_ul["what"] = ""
        di_ur = {}
        di_ur["name"] = ""
        di_ur["what"] = ""
        if il != -1:
            di_ul["name"] = self.units[il]
            di_ul["what"] = self.world.units[self.units[il]].what
        if ir != -1:
            di_ur["name"] = self.units[ir]
            di_ur["what"] = self.world.units[self.units[ir]].what
        return di,di_ul,di_ur
    

