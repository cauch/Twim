#    This file is part of TWIM.
#
#    TWIM is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    TWIM is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with TWIM.  If not, see <http://www.gnu.org/licenses/>.

from random import *
import time
from twim_world import *
from twim_util import *
import os
import pickle

class Server():
    def __init__(self,g):
        """
        Main class for the server part of the game

        arguments:
           g: T_Server class, in order to access the network infrastructure
        """
        #server core related variables:
        self.g = g                            #T_Server class
        self.g.treat_receiveddata = self.Treat_receiveddata #fct called when a message is received
        self.status = 0                       #status: 0=init, 1=play, ...
        self.player = {}                      #dict of "player name":["player pass word",player_number]
        self.n_player = 0                     #number of current player
        #game environment related variables
        self.world = None
        self.colors = []
        #communication and server game related variables
        self.updatenotice = []                #list of list of the update to do
        self.timer_delay = 0.02               #for the timer, the minimum delay between two ticks
        self.timer_lasttime = -1              #for the timer, the lasttime when the tick happened
        self.time = 0                         #time in the game
        self.sessionname = "sess%s_%03i"%(time.strftime("%Y%m%d%H%M"),random.random()*999) #session name
        self.file_to_load = ""

        self.modules_unit,self.modules_decor = Get_ressources()#from util
        self.savedir = Get_savedir()#from util

    def __getstate__(self):
        """
        Veto attributes that should not be saved in files
        """
        pickle_veto = ["g",
                       "modules_unit",
                       "modules_decor",
                       "file_to_load",
                       "player",
                       "n_player"
                   ]
        return {k: v for k, v in self.__dict__.iteritems() if k not in pickle_veto}

    def Loop(self):
        """
        The reactor in twim_network has created a thread that call this function every 0.1 second
        """
        if self.world:
            if self.world.nature:
                self.world.nature.Loop()

    def Treat_receiveddata(self,data):
        """
        Function called when data is received
        If no error, returns the correctly formated line
        Uses subtreat_ function to clarify the steps

        arguments:
           data: data text received by the server from the client
        return:
           text to be send back to the client, correctly formated or starting by 'E'
        """
        
        return_data = ""

        #packet format:
        # server_password @ player_name @ player_password @ data
        d = data.split("@")
        #check the password
        if self.Subtreat_checkpassword(d[0]) == 0:
            return_data = "E1"
        elif len(d) != 4:
            #if uncorrectly formed
            return_data = "E2"
        else:
            player_name = d[1]            
            #check if new player
            r = self.Subtreat_newplayer(d)
            if r:
                #this is a new player
                return_data = r
            elif self.player[player_name][0] != d[2]:
                #this is not a new player, but the password is incorrect
                return_data = "E incorrect player password"
            else:
                player_number = self.player[player_name][1]
                #different requests
                if d[3] == "UPInit":
                    #initial update init
                    if not self.file_to_load:
                        if not self.world: self.Create_environ()
                        self.world.Create_newplayer(player_number,player_name)
                    else:
                        self.Load()
                        self.world.Link_playertoloaded(player_number,player_name)
                    return_data = "UPIok"+self.Dump_environ(player_number)
                elif d[3] == "UP":
                    #usual update request from the client
                    #first, apply the timer
                    if time.time()-self.timer_lasttime > self.timer_delay:
                        if self.timer_lasttime < 0: self.timer_lasttime = time.time()
                        else:
                            dt = time.time()-self.timer_lasttime
                            self.timer_lasttime = time.time()
                            self.Tick_timer(dt)
                    #then, return the modification
                    return_data = "UPok"+self.Update(player_number)
                elif d[3].startswith("CO"):
                    #send a command
                    com = d[3][2:].split(" ")[0]
                    arg = d[3][2:].split(" ")[1:]
                    return_data = "COok"
                    if "Command" in os.environ.get("TWIM_DEBUG",""):
                        if com == "save":
                            self.Save()
                        elif com == "attack" and arg == []:
                            rkn = self.world.units[player_name].room_keyname
                            info_dicor,info_dicoul,info_dicour = self.world.rooms[rkn].Get_info(player_name)
                            if info_dicoul:
                                arg = [info_dicoul["name"]]
                            elif info_dicour:
                                arg = [info_dicour["name"]]
                        else:
                            return_data = "COok"+self.Play_spell(com,arg,player_name)[0]
                    elif "FakePlay" in os.environ.get("TWIM_DEBUG",""):
                        return_data = "COok"+self.Fake_play(player_number,int(os.environ.get("TWIM_DEBUG","").split("FakePlay")[1][:2]))
                    elif com in self.world.spells.keys():
                        return_data = "COok"+self.Play_spell(com,arg,player_name)[0]
                elif d[3].startswith("RO"):
                    #return the room of the player
                    rkn = self.world.units[player_name].room_keyname
                    info_dicor,info_dicoul,info_dicour = self.world.rooms[rkn].Get_info(player_name)
                    return_data = "ROokroom="+repr(info_dicor)
                    return_data += "#unitl="+repr(info_dicoul)
                    return_data += "#unitr="+repr(info_dicour)
                    info_dico = self.world.units[player_name].Get_info()
                    return_data += "#player="+repr(info_dico)
                elif d[3].startswith("DU"):
                    #ask for description of a unit with the following name
                    nam = d[3][2:].split(" ")[0]
                    arg = d[3][2:].split(" ")[1:]
                    return_data = "DUok"
                    if nam in self.world.units.keys():
                        return_data = "DUokview="+repr(self.world.units[nam].Get_info())
                    else:
                        print "it is another player ? was it deleted in the meantime ?"
                else:
                    print "todo"
                    return_data = "Etodo"

        #format the returning data correctly
        if return_data[0] != "E":
            return self.g.password+"@server@mdp@"+return_data
        else:
            return return_data

    def Subtreat_checkpassword(self,d):
        """
        Subfunction of treat_data
        Checks if the game password is correct

        arguments:
           d: splitted packed
        return:
           boolean, 0 if the password is incorrect
        """
        if d != self.g.password: return 0
        else: return 1

    def Subtreat_newplayer(self,d):
        """
        Subfunction of treat_data
        Checks if the player is new, and if yes, adds it if possible

        arguments:
           d: splitted packed
        return:
           text: empty if the player already exist, otherwise, text to be sent back to the client
        """
        player_name,player_mpd = d[1:3]
        if not(player_name in self.player):
            if player_name == "server":
                return "NPno. already taken"
            #not in the list, check what he want to be
            if d[3] == "WTP":
                #he want to play
                if self.n_player < 2:
                    self.player[player_name] = [d[2],self.n_player]
                    self.n_player += 1
                    self.updatenotice.append([])
                    return "NPok"
                else:
                    return "NPno. server full"
            elif d[3] == "WTW":
                #he want to watch
                print "todo"
                return "Etodo"
            return "NPno. what do you want?"
        return ""

    def Create_environ(self):
        """
        Function called when the map is not defined yet (at the beginning when the first player connects)
        Creates the environment for the game.
        """
        #environment:
        self.colors = self.Create_color()
        self.world = World(self)
        self.world.Create_structures()
        self.world.Create_spells()
        if "BigWorld" in os.environ.get("TWIM_DEBUG",""):
            self.world.Generate_room_around("0_0",150,5)
        else:
            self.world.Generate_room_around("0_0",50,5)
        #self.world.Print_rooms()

    def Create_color(self):
        #this loop can be infinite ?
        colors = []
        #r = randint(100,200)
        #c1 = [50+r,250-r,50]
        #c2 = [50+(r/2),50+(r/2),50]
        #c3 = [50,50+r,250-r]
        #c4 = [50,50+(r/2),50+(r/2)]
        #c5 = [250-r,50,50+r]
        #c6 = [50+(r/2),50,50+(r/2)]
        #colors = [c1,c2,c3,c4,c5,c6]
        #shuffle(colors)
        
        colors = []
        wd = 50
        while len(colors) < 6 and wd > 0:
            norm = randint(350,500)
            r = [randint(50,250),randint(50,250),randint(50,250)]
            rf = norm*1./(r[0]+r[1]+r[2])
            r = [int(r[0]*rf),int(r[1]*rf),int(r[2]*rf)]
            if max(r) > 255:
                continue
            ok = 1
            for l in colors:
                dr = sqrt((l[0]-r[0])**2 + (l[1]-r[1])**2 + (l[2]-r[2])**2) 
                if dr < 100:
                    ok = 0
            if ok:
                colors.append(r[:])
            else:
                wd += -1
        if wd <= 0:
            return self.Create_color()
        return colors


    def Dump_environ(self,player_number):
        """
        Returns all the environment variables in a text that can be sent back to the client

        arguments:
           player_number: number of the player to which this data is sent
        return:
           text to be sent back to the client
        """
        t = ""
        #to dump:
        # 1) colors
        # 2) intro text
        # 3) usual room, player, left-right unit
        cl = self.colors
        t = VERSION
        t += "#"+repr(cl)
        t += "#!T{'type':'first','1':'%s','2':'%s'}"%(self.world.ispells[1][0],self.world.ispells[2][0])
        player_name = self.world.iplayers[player_number]
        x = self.world.units[player_name].x
        y = self.world.units[player_name].y
        info_dicor,info_dicoul,info_dicour = self.world.rooms["%i_%i"%(x,y)].Get_info(player_name)
        t += "#room="+repr(info_dicor)
        t += "#unitl="+repr(info_dicoul)
        t += "#unitr="+repr(info_dicour)
        info_dico = self.world.units[player_name].Get_info()
        t += "#player="+repr(info_dico)
        t += "#sessionname="+repr(self.sessionname)

        return t
    
    def Update(self,player_number):
        """
        Checks inside the updatenotice[player_number] list and informs the client

        arguments:
           player_number: number of the client which asks if there are updates
        return:
           text to be sent back to the client        
        """
        return_text = ""
        #transmit game time
        return_text = "#time="+repr(int(self.time))
        #transmit info in the queue (just the last occurance if doubles)
        li = self.updatenotice[player_number][:]
        li.reverse()
        what_is_done = []
        for utext in li:
            t = utext
            for tt in t.split("#"):
                if tt.count("=") == 0:
                    continue
                ind = tt.split("=")[0]
                if ind in what_is_done:
                    continue
                else:
                    what_is_done.append(ind)
                    return_text += "#"+tt
            self.updatenotice[player_number].remove(utext)
        return_text = return_text[1:] #remove the first '#'
        return return_text

    def Tick_timer(self,dt):
        """
        Timer function
        Triggered by communication with client
        (so, the delay between each tick is not constant, and is given by the argument) 

        arguments:
           dt: the time between now and the last time this function has been called
        """
        self.time += dt

    def Save(self):
        """
        Save the whole session in a file
        """
        print "the current world is saved in",self.savedir

        sfn = os.path.join( self.savedir , "%s.twim_save"%(self.sessionname) )
        pickle.dump(self, open(sfn, "wb"))

    def Load(self):
        sfn = self.file_to_load
        attr = self.__getstate__().keys()
        ns = pickle.load( open(sfn, "rb"))
        for a in attr:
            setattr(self,a,getattr(ns,a))
        self.world.server = self
        self.world.modules_unit = self.modules_unit
        self.world.modules_decor = self.modules_decor
        for u in self.world.units:
            self.world.units[u].world = self.world
        for s in self.world.spells:
            self.world.spells[s].world = self.world
        for r in self.world.rooms:
            if self.world.rooms[r]:
                self.world.rooms[r].world = self.world

    def Kill_unit(self,unit_name):
        if unit_name not in self.world.units.keys():
            print "uh?"
        u = self.world.units[unit_name]
        if u.player_number != -1:
            print "wow, it's a player"
        n = self.world.units[unit_name].room_keyname
        lp = self.world.rooms[n].Get_players()
        for pl in lp:
            info_dicor,info_dicoul,info_dicour = self.world.rooms[n].Get_info(pl)
            rd = ""
            if info_dicour.get("name") == unit_name:
                rd = 'killed=[2,"%s"]'%unit_name
            if info_dicoul.get("name") == unit_name:
                rd = 'killed=[0,"%s"]'%unit_name
            if pl == unit_name:
                rd = 'killed=[1,"%s"]'%unit_name
            pl_number = self.world.units[pl].player_number
            if rd and pl_number != -1:
                self.updatenotice[pl_number].append(rd)
        self.world.rooms[n].Remove_unit(unit_name)
        del self.world.units[unit_name]

    def Play_spell(self,spell_name,arg,player_name):
        unit_involved = {}
        unit_involved[player_name] = {"statusH":self.world.units[player_name].statusH[:],
                                      "statusS":self.world.units[player_name].statusS[:]}
        modifier = {}
        re = self.world.spells[spell_name].Play(arg,player_name,unit_involved,modifier)
        if re[1] >= 1:
            #the spell is successful, apply it
            r2n = []
            for pn in unit_involved:
                if pn in self.world.rooms.keys():
                    #it's a room -> to be notified
                    r2n.append(pn)
                else:
                    #it's an unit -> to update the status
                    self.world.units[pn].statusH = unit_involved[pn]["statusH"][:]
                    self.world.units[pn].statusS = unit_involved[pn]["statusS"][:]
                    if unit_involved[pn].get("knowledge",0) and self.world.units[pn].player_number == -1:
                        #if it's not a player, add some info ("has been attacked by ...")
                        #so the AI knows it
                        self.world.units[pn].knowledge += unit_involved[pn].get("knowledge",[])
                    if unit_involved[pn].get("killed",0):
                        print pn,"has been killed"
                        self.Kill_unit(pn)
            for pn in r2n:
                self.Notify_modification_inroom(pn)
        if re[1] >= 1:
            #notify the surrounding
            rkn = self.world.units[player_name].room_keyname
            info_dicor,info_dicoul,info_dicour = self.world.rooms[rkn].Get_info(player_name)
            if info_dicour.get("name"):
                urn = info_dicour.get("name")
                if self.world.units[urn].player_number != -1:
                    pl = self.world.units[urn].player_number
                    rd = 'spelled=[0,"%s"]'%spell_name
                    self.updatenotice[pl].append(rd)
                else:
                    #if it's not a player, add some info ("the player ... has played the spell ...")
                    #so the AI knows it
                    self.world.units[urn].knowledge += ["spell %s %s"%(player_name,spell_name)]
            if info_dicoul.get("name"):
                uln = info_dicoul.get("name")
                if self.world.units[uln].player_number != -1:
                    pl = self.world.units[uln].player_number
                    rd = 'spelled=[2,"%s"]'%spell_name
                    self.updatenotice[pl].append(rd)
                else:
                    #if it's not a player, add some info ("the player ... has played the spell ...")
                    #so the AI knows it
                    self.world.units[uln].knowledge += ["spell %s %s"%(player_name,spell_name)]
            #and himself:
            pl_number = self.world.units[player_name].player_number
            if pl_number != -1:
                self.updatenotice[pl_number].append('spelled=[1,"%s"]'%spell_name)

        return re[0],re[1]


    def Change_player_status(self,player_name,val_S,val_H,negative,option=0):
        """
        Change the status of the player (or a unit)

        arguments:
           player_name: the name of the player
           val_S: the vector values to be added to statusS 
           val_H: the vector values to be added to statusH 
           negative: if 0, do nothing particular
                     if 1, something < 0 is set to 0
                     if 2, something < 0 cancel the action
                     if 3, something < 0 cancel the action FOR THIS COLOR
           option: if 1, the value is not added, but is replacing the previous value

        return:
           0: everything is fine
           1: something went < 0
           2: something reached 0 (but not below)
           3: something went < 0 and reached 0
           4: nothing has changed
        """

        clplS = self.world.units[player_name].statusS[:]
        clplH = self.world.units[player_name].statusH[:]

        if option == 1:
            clplS = [0,0,0,0,0,0]
            clplH = [0,0,0,0,0,0]
        below0 = []
        reach0 = 0
        for i in range(6):
            clplS[i] += val_S[i]
            clplH[i] += val_H[i]
            if (clplS[i] == 0 and val_S[i] != 0) or (clplH[i] == 0 and val_H[i] != 0):
                reach0 = 1
            if (clplS[i] < 0 and val_S[i] != 0) or (clplH[i] < 0 and val_H[i] != 0):
                below0.append(i)
            if negative > 0:
                if clplS[i] < 0:
                    clplS[i] = 0
                if clplH[i] < 0:
                    clplH[i] = 0
        below0i = 0
        if below0: below0i = 1
        if negative == 2 and below0:
            return below0i+(2*reach0)
        if negative == 3 and below0:
            for i in below0:
                if clplS[i] == 0: clplH[i] = self.world.units[player_name].statusH[i]
                elif clplH[i] == 0: clplS[i] = self.world.units[player_name].statusS[i]

        if self.world.units[player_name].statusS == clplS[:] and self.world.units[player_name].statusH == clplH[:]:
            return 4

        self.world.units[player_name].statusS = clplS[:]
        self.world.units[player_name].statusH = clplH[:]
        n = self.world.units[player_name].room_keyname
        self.Notify_modification_inroom(n)
        return below0i+(2*reach0)

    def Move_player(self,player_name,room_key):
        """
        Move the player from a room to another
        """
        n = self.world.units[player_name].room_keyname

        self.world.rooms[n].Remove_unit(player_name)
        self.world.rooms[room_key].Add_unit(player_name)

    def Notify_modification_player(self,player_name):
        """
        Add a notice update for the player about himself
        (don't worry if it's called several time, there is a cleaning procedure when sent)
        
        arguments:
           player_name
        """
        
        self.world.units[player_name].Clean_spelldelays(self)
        info_dico = self.world.units[player_name].Get_info()
        return_data = "player="+repr(info_dico)
        pl_number = self.world.units[player_name].player_number
        self.updatenotice[pl_number].append(return_data)


    def Notify_modification_inroom(self,rkn):
        """
        Add a notice update for all player in the same room
        
        arguments:
           rkn: room keyname
        """
        lp = self.world.rooms[rkn].Get_players()

        for pl in lp:
            info_dicor,info_dicoul,info_dicour = self.world.rooms[rkn].Get_info(pl)
            return_data = "room="+repr(info_dicor)
            return_data += "#unitl="+repr(info_dicoul)
            return_data += "#unitr="+repr(info_dicour)
            self.world.units[pl].Clean_spelldelays(self)
            info_dico = self.world.units[pl].Get_info()
            return_data += "#player="+repr(info_dico)
            if info_dicoul["name"]:
                return_data += "#viewl="+repr(self.world.units[info_dicoul["name"]].Get_info())
            if info_dicour["name"]:
                return_data += "#viewr="+repr(self.world.units[info_dicour["name"]].Get_info())
            pl_number = self.world.units[pl].player_number
            self.updatenotice[pl_number].append(return_data)

    def Fake_play(self,player_number,what="M"):
        #for debug only
        if what == "M" or what == 1:
            #just go to a random new place
            pn = self.world.iplayers[player_number]
            n = "%i_%i"%(self.world.units[pn].x,self.world.units[pn].y)
            nr = []
            for i in range(4):
                if i in self.world.rooms[n].exits:
                    r = self.world.Get_nextroom(n,i)
                    if r != None:
                        nr.append([i,r])
            random.shuffle(nr)
            arg = nr[0][1].name
            com = self.world.Find_spell([2],[],-1).name
            return self.Play_spell(com,[arg],pn)[0]
        if what == 2:
            #sent the message that someone is dead in position 4
            self.updatenotice[player_number].append('killed=[3,"test"]')
        return ""
